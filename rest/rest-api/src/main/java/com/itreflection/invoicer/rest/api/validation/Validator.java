package com.itreflection.invoicer.rest.api.validation;

import java.util.Map;

import com.itreflection.invoicer.rest.api.model.ResponseDto;

public interface Validator<T> {

  ResponseDto<T> validate(Map<Class, Object> params);
}
