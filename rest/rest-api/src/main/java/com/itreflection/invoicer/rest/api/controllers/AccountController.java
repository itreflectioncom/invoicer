package com.itreflection.invoicer.rest.api.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.rest.api.model.ResponseDto;

@RequestMapping("/account")
public interface AccountController extends RestController<ResponseDto<AccountDto>> {

   @RequestMapping("/findByUsername/{username}")
   @ResponseBody
   ResponseDto<AccountDto> findByUserName(@PathVariable("username") String username);

   @RequestMapping("/delete/{id}")
   @ResponseBody
   void delete(@PathVariable("id") Long id);

   @RequestMapping("/getAll")
   @ResponseBody
   ResponseDto<List<AccountDto>> findAll();

   @RequestMapping(value = "/save}", method = RequestMethod.POST)
   @ResponseBody
   ResponseDto<AccountDto> save(@RequestBody AccountDto accountDto);

   @RequestMapping(value = "/register}", method = RequestMethod.POST)
   @ResponseBody
   ResponseDto<AccountDto> register(@RequestBody AccountDto accountDto);

   @RequestMapping(value = "/login}", method = RequestMethod.POST)
   @ResponseBody
   ResponseDto<AccountDto> login(@RequestBody AccountDto accountDto);


}
