/*
 * Amadeus Confidential Information:
 * Unauthorized use and disclosure strictly forbidden.
 * @1998-2016 - Amadeus s.a.s - All Rights Reserved.
 */
package com.itreflection.invoicer.rest.impl.controllers;

import org.springframework.security.access.prepost.PreAuthorize;

@PreAuthorize("isAuthenticated() && hasAuthority('USER')")
public abstract class SecuredController {
}
