package com.itreflection.invoicer.rest.impl.controllers;

/**
 * Created by sscode on 2017-03-14.
 */
public enum ControllerErrorCodes {
  ACCOUNT_USERNAME_EMPTY("100", "account.username.empty"),
  ACCOUNT_USERNAME_WRONGSIZE("101", "account.username.wrongSize"),
  ACCOUNT_USERNAME_NOTVALIDCHARS("102", "account.username.notValidChars"),
  ACCOUNT_EMAIL_EMPTY("103", "account.email.empty"),
  ACCOUNT_EMAIL_WRONGSIZE("104", "account.email.wrongSize"),
  ACCOUNT_EMAIL_NOTVALIDCHARS("105", "account.email.notValidChars"),
  ACCOUNT_PASSWORD_EMPTY("106", "account.password.empty"),
  ACCOUNT_PASSWORD_WRONGSIZE("107", "account.password.wrongSize"),

  CONTROLLER_DELETE_IMPOSSIBLE("110", "controller.delete.account.impossible"),
  CONTROLLER_UPDATE_IMPOSSIBLE("111", "controller.update.account.impossible");

  private String code;
  private String msg;

  ControllerErrorCodes(String code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  public String getCode() {
    return code;
  }

  public String getMsg() {
    return msg;
  }

}
