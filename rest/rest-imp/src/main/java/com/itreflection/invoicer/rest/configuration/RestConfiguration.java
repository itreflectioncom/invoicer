package com.itreflection.invoicer.rest.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.itreflection.invoicer.rest.impl" })
public class RestConfiguration {
}
