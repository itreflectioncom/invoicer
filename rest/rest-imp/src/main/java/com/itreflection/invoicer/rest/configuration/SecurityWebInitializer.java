package com.itreflection.invoicer.rest.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by sscode on 2017-03-28.
 */
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {
}
