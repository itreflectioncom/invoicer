package com.itreflection.invoicer.rest.impl.configuration;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by sscode on 2017-04-02.
 */
@Service
public class BCryptEncoder implements PasswordEncoder {

  PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  @Override
  public String encode(CharSequence charSequence) {
    return passwordEncoder.encode(charSequence);
  }

  @Override
  public boolean matches(CharSequence charSequence, String s) {
    String passwordToCheck = encode(charSequence);
    if(passwordToCheck.equals(s)) {
      return true;
    }
    return false;
  }
}
