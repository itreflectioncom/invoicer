package com.itreflection.invoicer.rest.impl.controllers;

import static com.itreflection.invoicer.rest.impl.controllers.ControllerErrorCodes.*;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.api.validation.Error;
import com.itreflection.invoicer.rest.api.validation.Validator;

@Component
public class AccountValidatorImpl implements Validator<AccountDto> {

  public static final String accountRegexp = "^[A-Za-z0-9+_.-]+";
  public static final String emailRegexp = "^[A-Za-z0-9+_.-]+@(.+)$";

  public static final Integer usernameMaxSize = 30;
  public static final Integer usernameMinSize = 3;
  public static final Integer emailMaxSize = 40;
  public static final Integer emailMinSize = 10;
  public static final Integer passwordMaxSize = 30;
  public static final Integer passwordMinSize = 3;

  @Override
  public ResponseDto<AccountDto> validate(Map<Class, Object> params) {
    ResponseDto<AccountDto> responseDto = new ResponseDto<>();
    AccountDto accountDto = (AccountDto) params.get(AccountDto.class);
    responseDto.setObject(accountDto);

    //validation

    validateUsername(accountDto, responseDto);

    validateEmail(accountDto, responseDto);

    validatePassword(accountDto, responseDto);

    return responseDto;
  }

  private void validateUsername(AccountDto accountDto, ResponseDto<AccountDto> responseDto) {
    String username = accountDto.getUsername();

    validationEmptyFields(responseDto, username, ACCOUNT_USERNAME_EMPTY);

    validationWrongSizeFields(responseDto, username, usernameMaxSize, usernameMinSize,
        ACCOUNT_USERNAME_WRONGSIZE);

    if (!StringUtils.isEmpty(username)) {
      if (!username.matches(accountRegexp)) {
        responseDto.getErrors()
            .add(new Error(ACCOUNT_USERNAME_NOTVALIDCHARS.getCode(), ACCOUNT_USERNAME_NOTVALIDCHARS.getMsg()));
      }
    }
  }

  private void validateEmail(AccountDto accountDto, ResponseDto<AccountDto> responseDto) {
    String email = accountDto.getEmail();

    validationEmptyFields(responseDto, email, ACCOUNT_EMAIL_EMPTY);

    validationWrongSizeFields(responseDto, email, emailMaxSize, emailMinSize, ACCOUNT_EMAIL_WRONGSIZE);

    if (!StringUtils.isEmpty(email)) {
      if (!email.matches(emailRegexp)) {
        responseDto.getErrors()
            .add(new Error(ACCOUNT_EMAIL_NOTVALIDCHARS.getCode(), ACCOUNT_EMAIL_NOTVALIDCHARS.getMsg()));
      }
    }
  }

  private void validatePassword(AccountDto accountDto, ResponseDto<AccountDto> responseDto) {
    String password = accountDto.getPassword();

    validationEmptyFields(responseDto, password, ACCOUNT_PASSWORD_EMPTY);

    validationWrongSizeFields(responseDto, password, passwordMaxSize, passwordMinSize, ACCOUNT_PASSWORD_WRONGSIZE);
  }

  private void validationEmptyFields(ResponseDto<AccountDto> responseDto, String field,
      ControllerErrorCodes errorCodes) {
    if (StringUtils.isEmpty(field)) {
      addErrorToResponseDto(responseDto, errorCodes);
    }
  }

  private void validationWrongSizeFields(ResponseDto<AccountDto> responseDto, String field, Integer maxSize,
      Integer minSize, ControllerErrorCodes errorCodes) {
    if (StringUtils.length(field) < minSize || StringUtils.length(field) > maxSize) {
      addErrorToResponseDto(responseDto, errorCodes);
    }
  }

  private void addErrorToResponseDto(ResponseDto<AccountDto> responseDto, ControllerErrorCodes errorCodes) {
    responseDto.getErrors().add(new Error(errorCodes.getCode(), errorCodes.getMsg()));
  }

}

