package com.itreflection.invoicer.rest.impl.controllers;

import static com.itreflection.invoicer.rest.impl.controllers.ControllerErrorCodes.CONTROLLER_UPDATE_IMPOSSIBLE;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.api.validation.Error;
import com.itreflection.invoicer.rest.api.validation.Validator;

/**
 * Created by sscode on 2017-04-20.
 */
@Component
public class AccountSaveValidatorImpl implements Validator<AccountDto> {

  @Autowired
  AccountRepositoryService repositoryService;

  @Override
  public ResponseDto<AccountDto> validate(Map<Class, Object> params) {

    AccountDto accountDto = (AccountDto) params.get(AccountDto.class);

    Long idAccount = accountDto.getId();

    ResponseDto<AccountDto> responseDto = getUser();

    Long idLoggedUser = responseDto.getObject().getId();

    validateId(idLoggedUser, idAccount, responseDto);

    return responseDto;
  }

  private ResponseDto<AccountDto> getUser() {
    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    String username = user.getUsername();

    ResponseDto<AccountDto> responseDto = new ResponseDto<AccountDto>(repositoryService.findByName(username));
    return responseDto;
  }

  private void validateId(Long idLoggedUser, Long idAccount, ResponseDto<AccountDto> responseDto) {
    List<AuthorityType> authorities = responseDto.getObject().getAuthorities();

    for (AuthorityType authorityType : authorities) {
      if (authorityType == AuthorityType.ADMIN_UPDATE_ACCOUNT || idLoggedUser == idAccount) {
        return;
      }
    }

    responseDto.getErrors()
        .add(new Error(CONTROLLER_UPDATE_IMPOSSIBLE.getCode(), CONTROLLER_UPDATE_IMPOSSIBLE.getMsg()));
    throw new AccessDeniedException("Update account is impossible!");

  }
}
