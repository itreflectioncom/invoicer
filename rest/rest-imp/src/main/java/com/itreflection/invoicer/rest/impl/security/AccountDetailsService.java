package com.itreflection.invoicer.rest.impl.security;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;

@Transactional
@Service
public class AccountDetailsService implements UserDetailsService {

  private final AccountRepositoryService accountRepositoryService;

  @Autowired
  public AccountDetailsService(AccountRepositoryService accountRepositoryService) {
    this.accountRepositoryService = accountRepositoryService;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    try {
      AccountDto accountDto = accountRepositoryService.findByName(username);
      if (accountDto == null) {
        return null;
      }
      return new User(accountDto.getUsername(), accountDto.getPassword(), getAuthorities(accountDto));
    } catch (Exception e) {
      throw new UsernameNotFoundException("User not found");
    }
  }

  private Set<GrantedAuthority> getAuthorities(AccountDto accountDto) {
    Set<GrantedAuthority> authorities = new HashSet<>();
    accountDto.getAuthorities().forEach(item->authorities.add(new SimpleGrantedAuthority(item.toString())));


//    GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(accountDto.getAuthorityType().toString());
//    authorities.add(grantedAuthority);
    return authorities;
  }

}
