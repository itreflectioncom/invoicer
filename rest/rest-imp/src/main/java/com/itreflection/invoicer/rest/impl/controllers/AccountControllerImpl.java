package com.itreflection.invoicer.rest.impl.controllers;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.controllers.AccountController;
import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.api.validation.Validate;

@RestController
public class AccountControllerImpl extends SecuredController implements AccountController {

  private final AccountRepositoryService accountRepositoryService;

  public AccountControllerImpl(AccountRepositoryService accountRepositoryService) {
    this.accountRepositoryService = accountRepositoryService;
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  public ResponseDto<AccountDto> findByUserName(@PathVariable("username") String username) {
    return new ResponseDto(accountRepositoryService.findByName(username));
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @Validate({AccountDeleteValidatorImpl.class})
  public void delete(@PathVariable("id") Long id) {
    accountRepositoryService.delete(id);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  public ResponseDto<List<AccountDto>> findAll() {
    return new ResponseDto(accountRepositoryService.findAll());
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @Validate({ AccountValidatorImpl.class, AccountSaveValidatorImpl.class })
  public ResponseDto<AccountDto> save(@RequestBody AccountDto accountDto) {
    AccountDto account = accountRepositoryService.save(accountDto);

    return new ResponseDto(account);
  }

  @PreAuthorize("permitAll()")
  public ResponseDto<AccountDto> register(AccountDto accountDto) {
    //  accountDto.setAuthorityType(AuthorityType.ADMIN);
    return save(accountDto);
  }

  @PreAuthorize("permitAll()")
  public ResponseDto<AccountDto> login(AccountDto accountDto) {
    return null;
  }

}
