package com.itreflection.invoicer.rest.impl.controllers;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.api.validation.Error;

/**
 * Created by sscode on 2017-03-01.
 */

@RunWith(MockitoJUnitRunner.class)
public class AccountValidatorImplTest {

  @Mock
  AccountDto accountDto;

  @InjectMocks
  AccountValidatorImpl accountValidator;

  @Before
  public void initialize() {
    accountDto = new AccountDto();
  }

  @Test
  public void shouldReturnErrorUsernameEmpty() {

    String username = null;
    String password = "aaaaaaaaaaaaaa";
    String email = "test@test.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("100", "account.username.empty"));

  }

  @Test
  public void shouldReturnErrorUsernameWrongSize() {

    String username = "aa";
    String password = "aaaaaaaaaaaaaa";
    String email = "test@test.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("101", "account.username.wrongSize"));

  }

  @Test
  public void shouldReturnErrorUsernameNotValidChars() {

    String username = "@#$%&**(";
    String password = "aaaaaaaaaaaaaa";
    String email = "test@test.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("102", "account.username.notValidChars"));

  }

  @Test
  public void shouldReturnErrorEmailEmpty() {

    String username = "aaaaa";
    String password = "aaaaaaaaaaaaaa";
    String email = null;

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("103", "account.email.empty"));

  }

  @Test
  public void shouldReturnErrorEmailWrongSize() {

    String username = "aaaaaaa";
    String password = "aaaaaaaaaaaaaa";
    String email = "a@a.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("104", "account.email.wrongSize"));

  }

  @Test
  public void shouldReturnErrorEmailNotValidChars() {

    String username = "aaaaaaa";
    String password = "aaaaaaaaaaaaaa";
    String email = "!!!test.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("105", "account.email.notValidChars"));

  }

  @Test
  public void shouldReturnErrorPasswordEmpty() {

    String username = "aaaaa";
    String password = null;
    String email = "test@test.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("106", "account.password.empty"));

  }

  @Test
  public void shouldReturnErrorPasswordWrongSize() {

    String username = "aaaaaaa";
    String password = "aa";
    String email = "test@test.com";

    accountDto.setUsername(username);
    accountDto.setPassword(password);
    accountDto.setEmail(email);

    Map<Class, Object> paramsMap = new HashMap<>();
    paramsMap.put(AccountDto.class, accountDto);

    ResponseDto<AccountDto> responseAccountDto = Mockito.mock(ResponseDto.class);

    ResponseDto<AccountDto> returnedAccountWithError = accountValidator.validate(paramsMap);

    assertThat(returnedAccountWithError.getErrors().get(0))
        .isEqualTo(new Error("107", "account.password.wrongSize"));

  }

}
