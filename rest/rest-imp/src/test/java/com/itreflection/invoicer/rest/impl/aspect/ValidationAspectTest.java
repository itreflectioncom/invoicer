package com.itreflection.invoicer.rest.impl.aspect;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.api.validation.Validate;
import com.itreflection.invoicer.rest.api.validation.Validator;

@RunWith(MockitoJUnitRunner.class)
public class ValidationAspectTest {

  @Mock
  private ApplicationContext applicationContext;

  @InjectMocks
  private ValidationAspect validationAspect;

  @Test
  public void shouldInvokeValidator() throws Throwable {
    //given
    ProceedingJoinPoint joinPoint = mock(ProceedingJoinPoint.class);
    Validate validate = mock(Validate.class);
    MethodSignature methodSignature = mock(MethodSignature.class);
    TestValidatorImpl testValidator = mock(TestValidatorImpl.class);

    when(joinPoint.getSignature()).thenReturn(methodSignature);
    when(joinPoint.getArgs()).thenReturn(new String[] { "param1" });
    when(methodSignature.getParameterTypes()).thenReturn(new Class[] { String.class });
    when(validate.value()).thenReturn(new Class[] { TestValidatorImpl.class });
    when(testValidator.validate(anyMap())).thenReturn(mock(ResponseDto.class));
    when(applicationContext.getBean(TestValidatorImpl.class)).thenReturn(testValidator);

    //when
    Object result = validationAspect.validate(joinPoint, validate);

    //then
    verify(testValidator).validate(anyMap());
    assertThat(result).isNull();
  }

  @Test
  public void shouldInvokeTwoOrMoreValidators() throws Throwable {
    //given
    ProceedingJoinPoint joinPoint = mock(ProceedingJoinPoint.class);
    Validate validate = mock(Validate.class);
    MethodSignature methodSignature = mock(MethodSignature.class);
    TestValidatorImpl testValidator = mock(TestValidatorImpl.class);
    TestValidatorTwoImpl testValidatorTwo = mock(TestValidatorTwoImpl.class);

    when(joinPoint.getSignature()).thenReturn(methodSignature);
    when(joinPoint.getArgs()).thenReturn(new String[] { "param1" });
    when(methodSignature.getParameterTypes()).thenReturn(new Class[] { String.class });
    when(validate.value()).thenReturn(new Class[] { TestValidatorImpl.class, TestValidatorTwoImpl.class });
    when(testValidator.validate(anyMap())).thenReturn(mock(ResponseDto.class));
    when(applicationContext.getBean(TestValidatorImpl.class)).thenReturn(testValidator);
    when(applicationContext.getBean(TestValidatorTwoImpl.class)).thenReturn(testValidatorTwo);

    //when
    Object result = validationAspect.validate(joinPoint, validate);

    //then
    verify(testValidator).validate(anyMap());
    assertThat(result).isNull();
  }

  @Test(expected = IllegalStateException.class)
  public void shouldThrowIllegalStateException() throws Throwable {
    MethodSignature methodSignature = mock(MethodSignature.class);
    ProceedingJoinPoint joinPoint = mock(ProceedingJoinPoint.class);
    Validate validate = mock(Validate.class);
    TestValidatorImpl validator = mock(TestValidatorImpl.class);

    when(joinPoint.getSignature()).thenReturn(methodSignature);
    when(joinPoint.getArgs()).thenReturn(null);

    Object result = validationAspect.validate(joinPoint, validate);
  }

  @Test
  public void shouldReturnResponseWithoutProceed() throws Throwable {
    ProceedingJoinPoint joinPoint = mock(ProceedingJoinPoint.class);
    Validate validate = mock(Validate.class);
    MethodSignature methodSignature = mock(MethodSignature.class);
    TestValidatorImpl validator = mock(TestValidatorImpl.class);

    ResponseDto responseDto = new ResponseDto();

    when(joinPoint.getSignature()).thenReturn(methodSignature);
    when(joinPoint.getArgs()).thenReturn(new String[] { "param1" });
    when(methodSignature.getParameterTypes()).thenReturn(new Class[] { String.class });
    when(validate.value()).thenReturn(new Class[] { TestValidatorImpl.class });

    ArrayList<String> array = new ArrayList<String>();
    array.add("Error");
    responseDto.getErrors().add(array);

    when(applicationContext.getBean(TestValidatorImpl.class)).thenReturn(validator);
    when(validator.validate(anyMap())).thenReturn(responseDto);

    Object result = validationAspect.validate(joinPoint, validate);

    assertThat((ResponseDto) result).isEqualTo(responseDto);
  }

  class TestValidatorImpl implements Validator<String> {
    @Override
    public ResponseDto<String> validate(Map<Class, Object> params) {
      return new ResponseDto<>();
    }
  }

  class TestValidatorTwoImpl implements Validator<String> {

    @Override
    public ResponseDto<String> validate(Map<Class, Object> params) {
      return new ResponseDto<>();
    }
  }

}
