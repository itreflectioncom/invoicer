package com.itreflection.invoicer.rest.impl.controllers;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.model.ResponseDto;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerImplTest {

  @InjectMocks
  private AccountControllerImpl accountController;

  @Mock
  private AccountRepositoryService accountRepositoryService;

  @Test
  public void shouldSave() throws Exception {
    //given
    AccountDto accountDto = Mockito.mock(AccountDto.class);

    Mockito.when(accountRepositoryService.save(accountDto)).thenReturn(accountDto);

    //when
    ResponseDto<AccountDto> saved = accountController.save(accountDto);

    //then
    Mockito.verify(accountRepositoryService).save(accountDto);
    assertThat(saved.getObject()).isEqualTo(accountDto);

  }

  @Test
  public void shouldDelete() throws Exception {
    //given
    long id = 1L;

    //when
    accountController.delete(id);

    //then
    Mockito.verify(accountRepositoryService).delete(id);
  }

  @Test
  public void shouldFindAll() throws Exception {
    //given
    AccountDto accountDto = Mockito.mock(AccountDto.class);

    Mockito.when(accountRepositoryService.findAll()).thenReturn(Lists.newArrayList(accountDto));

    //when
    ResponseDto<List<AccountDto>> accounts = accountController.findAll();

    //then
    Mockito.verify(accountRepositoryService).findAll();
    assertThat(accounts.getObject()).isNotEmpty().contains(accountDto);
  }

  @Test
  public void shouldFindByUsername() throws Exception {
    //given
    AccountDto accountDto = Mockito.mock(AccountDto.class);
    String username = "username";

    Mockito.when(accountRepositoryService.findByName(username)).thenReturn(accountDto);

    //when
    ResponseDto<AccountDto> account = accountController.findByUserName(username);

    //then
    Mockito.verify(accountRepositoryService).findByName(username);
    assertThat(account.getObject()).isEqualTo(accountDto);
  }

}