package com.itreflection.invoicer.integration.configuration.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.itreflection.invoicer.rest.configuration.RestConfiguration;

@Configuration
@Import(RestConfiguration.class)
public class RestIntConfiguration {

}
