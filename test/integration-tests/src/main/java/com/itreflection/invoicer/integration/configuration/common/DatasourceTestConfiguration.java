package com.itreflection.invoicer.integration.configuration.common;

import java.io.IOException;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.itreflection.invoicer.config.JPAConfiguration;

@Configuration
@Import(JPAConfiguration.class)
public class DatasourceTestConfiguration {

  @Bean
  public DataSource dataSource() {
//    return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).setName("integration-test")
//                .addScript("db/ddl.sql").addScript("db/dml.sql")
//        .build();

    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.h2.Driver");
    dataSource.setUsername("invoicer");
    dataSource.setPassword("master");
    dataSource.setUrl("jdbc:h2:~/integration-test;INIT=create schema if not exists Queue");

    return dataSource;
  }

  @Bean
  public JdbcTemplate jdbcTemplate() throws IOException, SQLException {
    return new JdbcTemplate(dataSource());
  }

}
