/*
 * Amadeus Confidential Information:
 * Unauthorized use and disclosure strictly forbidden.
 * @1998-2016 - Amadeus s.a.s - All Rights Reserved.
 */
package com.itreflection.invoicer.integration.impl.rule;

import org.junit.rules.TestRule;

public interface LoginRule extends TestRule{
}
