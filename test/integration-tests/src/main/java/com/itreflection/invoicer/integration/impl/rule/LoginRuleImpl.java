/*
 * Amadeus Confidential Information:
 * Unauthorized use and disclosure strictly forbidden.
 * @1998-2016 - Amadeus s.a.s - All Rights Reserved.
 */
package com.itreflection.invoicer.integration.impl.rule;

import java.util.Arrays;
import java.util.List;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.security.SecurityService;

@Service
public class LoginRuleImpl extends TestWatcher implements LoginRule {

  public static final String TEST_USER = "testUser";
  public static final String TEST_PASSWORD = "password";
  public static final String TEST_EMAIL = "email@test.com";
  private final SecurityService securityService;
  private final AccountRepositoryService accountRepositoryService;

  @Autowired
  public LoginRuleImpl(SecurityService securityService, AccountRepositoryService accountRepositoryService) {
    this.securityService = securityService;
    this.accountRepositoryService = accountRepositoryService;
  }

  @Override
  protected void starting(Description description) {

    Login annotation = description.getAnnotation(Login.class);
    if (annotation != null) {
      if (annotation.create()) {
        resetUser(annotation);
        createUser(annotation.username(), annotation.password(), annotation.email(), annotation.authorities());
        securityService.login(annotation.username(), annotation.password());
        return;
      }
      return;
    }

    AccountDto account = accountRepositoryService.findByName(TEST_USER);
    if (account == null) {
      createDefaultAdminUser();
    }
    securityService.login(TEST_USER, TEST_PASSWORD);
  }

  private void createUser(String username, String password, String email, AuthorityType[] authorities) {
    List<AuthorityType> authoritiesList = Arrays.asList(authorities);
    AccountDto account = new AccountDto(username, email, password);
    account.getAuthorities().addAll(authoritiesList);
    accountRepositoryService.save(account);
  }

  private void resetUser(Login annotation) {
    AccountDto account = accountRepositoryService.findByName(TEST_USER);
    if (account != null) {
      accountRepositoryService.delete(account.getId());
    }

    if (account == null) {
      createDefaultAdminUser();
    }
  }

  private void createDefaultAdminUser() {
    AccountDto account = new AccountDto(TEST_USER, TEST_EMAIL, TEST_PASSWORD);
    account.getAuthorities().add(AuthorityType.TEST_AUTHORITY);
    accountRepositoryService.save(account);
  }

  @Override
  protected void finished(Description description) {
    SecurityContextHolder.getContext().setAuthentication(null);
  }
}
