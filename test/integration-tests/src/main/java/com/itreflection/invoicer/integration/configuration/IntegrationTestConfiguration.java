package com.itreflection.invoicer.integration.configuration;

import org.junit.rules.RuleChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.web.WebAppConfiguration;

import com.itreflection.invoicer.integration.configuration.common.DatasourceTestConfiguration;
import com.itreflection.invoicer.integration.impl.rule.IntegrationRuleChainService;

@Configuration
@ComponentScan(basePackageClasses = DatasourceTestConfiguration.class, basePackages = {"com.itreflection.invoicer.integration.impl"})
@EnableAspectJAutoProxy
@WebAppConfiguration
public class IntegrationTestConfiguration {

  @Autowired
  @Bean
  public RuleChain integrationRuleChain(IntegrationRuleChainService integrationRuleChainService) {
    return integrationRuleChainService.getRuleChain();
  }

}
