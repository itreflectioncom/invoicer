/*
 * Amadeus Confidential Information:
 * Unauthorized use and disclosure strictly forbidden.
 * @1998-2016 - Amadeus s.a.s - All Rights Reserved.
 */
package com.itreflection.invoicer.integration.configuration.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.itreflection.invoicer.rest.configuration.SecurityConfig;

@Configuration
@Import(SecurityConfig.class)
public class SecurityIntConfiguration {
}
