/*
 * Amadeus Confidential Information:
 * Unauthorized use and disclosure strictly forbidden.
 * @1998-2016 - Amadeus s.a.s - All Rights Reserved.
 */
package com.itreflection.invoicer.integration.impl.rule;

import org.junit.rules.RuleChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntegrationRuleChainServiceImpl implements IntegrationRuleChainService {

  private final LoginRule loginRule;

  @Autowired
  public IntegrationRuleChainServiceImpl(LoginRule loginRule) {
    this.loginRule = loginRule;
  }

  @Override
  public RuleChain getRuleChain() {
    return RuleChain.outerRule(loginRule);
  }
}
