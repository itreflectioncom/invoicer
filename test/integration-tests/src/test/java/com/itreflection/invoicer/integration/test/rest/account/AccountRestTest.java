package com.itreflection.invoicer.integration.test.rest.account;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.itreflection.invoicer.integration.impl.rule.Login;
import com.itreflection.invoicer.integration.test.AbstractIntegrationTest;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.controllers.AccountController;
import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.impl.controllers.ControllerErrorCodes;

public class AccountRestTest extends AbstractIntegrationTest {

  @Autowired
  private AccountController accountController;

  @Autowired
  private AccountRepositoryService repositoryService;

  /*@Before
  public void init() {
    ResponseDto<List<AccountDto>> accounts = accountController.findAll();
    for (AccountDto account : accounts.getObject()) {
      repositoryService.delete(account.getId());
    }
  }*/

  @Login(username = "username14", password = "password", email = "test14_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldSaveAccount() throws Exception {
    //given
    String username = "userA";
    String email = "testA@email.com";
    String password = "password";
    AccountDto accountDto = repositoryService.findByName("username14");

    createAccountDto(accountDto, username, email, password);

    //when
    AccountDto response = accountController.save(accountDto).getObject();

    //then
    assertThat(response.getId()).isNotNull();
    assertThat(response.getUsername()).isEqualTo(username);
    assertThat(response.getEmail()).isEqualTo(email);
    assertThat(response.getPassword()).isEqualTo(password);
    assertThat(response.getCreated()).isNotNull();
    //assertThat(response.getUpdated()).isNotNull();
  }

  @Login(username = "username15", password = "password", email = "test15_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAccountInvalidUsername() throws Exception {
    //given
    String username = "username!!!";
    String email = "test@email.com";
    String password = "password";
    AccountDto accountDto = repositoryService.findByName("username15");

    createAccountDto(accountDto, username, email, password);
    //when
    ResponseDto<AccountDto> response = accountController.save(accountDto);

    //then
    assertThat(response.getErrors()).hasSize(1);
    assertThat(response.getErrors().get(0).getCode())
        .isEqualTo(ControllerErrorCodes.ACCOUNT_USERNAME_NOTVALIDCHARS.getCode());
  }

  @Login(username = "username16", password = "password", email = "test16_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAccountInvalidEmail() throws Exception {
    //given
    String username = "username";
    String email = "test!comcom";
    String password = "password";
    AccountDto accountDto = repositoryService.findByName("username16");

    createAccountDto(accountDto, username, email, password);
    //when
    ResponseDto<AccountDto> responseDto = accountController.save(accountDto);

    //then
    assertThat(responseDto.getErrors()).hasSize(1);
    assertThat(responseDto.getErrors().get(0).getCode())
        .isEqualTo(ControllerErrorCodes.ACCOUNT_EMAIL_NOTVALIDCHARS.getCode());
  }

  @Login(username = "username17", password = "password", email = "test17_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldReadSavedAccount() throws Exception {
    //given
    String username = "userE";
    String email = "testE@email.com";
    String password = "password";
    AccountDto accountDto = repositoryService.findByName("username17");

    createAccountDto(accountDto, username, email, password);

    AccountDto saved = accountController.save(accountDto).getObject();

    //when
    AccountDto response = accountController.findByUserName(username).getObject();

    //then
    assertThat(response.getId()).isEqualTo(saved.getId());
    assertThat(response.getUsername()).isEqualTo(saved.getUsername());
    assertThat(response.getEmail()).isEqualTo(saved.getEmail());
    assertThat(response.getPassword()).isEqualTo(saved.getPassword());
  }

  @Login(username = "username18", password = "password", email = "test18_mail@test.com",
         authorities = { AuthorityType.ADMIN, AuthorityType.ADMIN_DELETE_ACCOUNT })
  @Test
  public void shouldDeleteAccount() throws Exception {
    assertThat(accountController.findByUserName("username18")).isNotNull();

    //when
    accountController.delete(repositoryService.findByName("username18").getId());

    //then
    try {
      accountController.findByUserName("username18").getObject().getId();
    } catch (Exception e) {
      assertThat(e).isInstanceOf(NullPointerException.class);
    }

  }

  @Login(username = "username19", password = "password", email = "test19_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldUpdate() throws Exception {
    //given
    String username = "userB";
    String email = "testB@email.com";
    String password = "passpass";
    AccountDto accountDto = repositoryService.findByName("username19");

    AccountDto accountCheck = new AccountDto();
    accountCheck.setUsername(accountDto.getUsername());
    accountCheck.setEmail(accountDto.getEmail());
    accountCheck.setPassword(accountDto.getPassword());


    createAccountDto(accountDto, username, email, password);

    AccountDto saved = accountController.save(accountDto).getObject();

    //when

    //then
    assertThat(accountDto.getId()).isEqualTo(saved.getId());
    assertThat(accountCheck.getUsername()).isNotEqualTo(saved.getUsername());
    assertThat(accountCheck.getPassword()).isNotEqualTo(saved.getPassword());
    assertThat(accountCheck.getEmail()).isNotEqualTo(saved.getEmail());
    assertThat(accountDto.getCreated()).isNotNull();
    //assertThat(response.getUpdated()).isNotNull();
  }

  @Login(username = "username20", password = "password", email = "test20_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldGetAll() throws Exception {
    //given
    String username = "userC";
    String username2 = "userD";
    String email = "testC@email.com";
    String email2 = "test2D@email.com";
    String password = "password";
    AccountDto accountDto = new AccountDto();
    AccountDto accountDto2 = new AccountDto();

    accountDto.setUsername(username);
    accountDto2.setUsername(username2);

    accountDto.setEmail(email);
    accountDto2.setEmail(email2);

    accountDto.setPassword(password);
    accountDto2.setPassword(password);

    repositoryService.save(accountDto);
    repositoryService.save(accountDto2);

    //when
    List<AccountDto> response = accountController.findAll().getObject();

    //then
    assertThat(response).isNotEmpty();
  }

  private void createAccountDto(AccountDto accountDto, String username, String email, String password) {
    accountDto.setUsername(username);
    accountDto.setEmail(email);
    accountDto.setPassword(password);
  }
}
