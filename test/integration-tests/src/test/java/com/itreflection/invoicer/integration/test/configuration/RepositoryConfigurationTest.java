package com.itreflection.invoicer.integration.test.configuration;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import com.itreflection.invoicer.integration.test.AbstractIntegrationTest;

public class RepositoryConfigurationTest extends AbstractIntegrationTest {

  @Autowired
  private PlatformTransactionManager tx;

  @Test
  public void testConfig() throws Exception {
    assertThat(tx).isNotNull();
  }
}
