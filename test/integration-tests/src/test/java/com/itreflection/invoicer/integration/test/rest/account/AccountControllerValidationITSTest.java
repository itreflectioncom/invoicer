package com.itreflection.invoicer.integration.test.rest.account;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.itreflection.invoicer.integration.impl.rule.Login;
import com.itreflection.invoicer.integration.test.AbstractIntegrationTest;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.controllers.AccountController;
import com.itreflection.invoicer.rest.api.model.ResponseDto;
import com.itreflection.invoicer.rest.api.validation.Error;

public class AccountControllerValidationITSTest extends AbstractIntegrationTest {

  public static final String USERNAME = "test_user1";
  public static final String EMAIL = "testtest_email@test.com";
  public static final String PASSWORD = "password";

  @Autowired
  private AccountController accountController;

  @Autowired
  private AccountRepositoryService repositoryService;

  @Login(username = "username13", password = "password", email = "test13_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldSaveAndReadAccount() throws Exception {
    //given
    AccountDto accountDto = repositoryService.findByName("username13");

    //when
    ResponseDto<AccountDto> account = accountController.save(accountDto);

    //then
    assertThat(accountController.findByUserName("username13").getObject().getId()).isNotNull();
    assertThat(accountController.findByUserName("username13")).isNotNull();

  }

  @Login(username = "username5", password = "password", email = "test5_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolateUsernameEmptyError() throws Exception {
    //given
    AccountDto accountDto = repositoryService.findByName("username5");
    accountDto.setUsername(null);

    //when
    ResponseDto<AccountDto> account = accountController.save(accountDto);

    //then

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("100", "account.username.empty"));
  }

  @Login(username = "username6", password = "password", email = "test6_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolateUsernameSizeError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username6");
    accountDto.setUsername("Aa");

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("101", "account.username.wrongSize"));
  }

  @Login(username = "username7", password = "password", email = "test7_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolateUsernameNotValidCharsError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username7");
    accountDto.setUsername("@$#%^&*");

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("102", "account.username.notValidChars"));
  }

  @Login(username = "username8", password = "password", email = "test8_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolateEmailEmptyError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username8");
    accountDto.setEmail(null);

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("103", "account.email.empty"));
  }

  @Login(username = "username9", password = "password", email = "test9_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolateEmailSizeError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username9");
    accountDto.setEmail("a@a.a");

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("104", "account.email.wrongSize"));
  }

  @Login(username = "username10", password = "password", email = "test10_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolateEmailNotValidCharsError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username10");
    accountDto.setEmail("aa.aa)(&^%");

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("105", "account.email.notValidChars"));
  }

  @Login(username = "username11", password = "password", email = "test11_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolatePasswordEmptyError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username11");
    accountDto.setPassword(null);

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("106", "account.password.empty"));
  }

  @Login(username = "username12", password = "password", email = "test12_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotSaveAndViolatePasswordSizeError() throws Exception {
    AccountDto accountDto = repositoryService.findByName("username12");
    accountDto.setPassword("Aa");

    ResponseDto<AccountDto> account = accountController.save(accountDto);

    assertThat(account.getErrors().get(0)).isEqualTo(new Error("107", "account.password.wrongSize"));
  }

}
