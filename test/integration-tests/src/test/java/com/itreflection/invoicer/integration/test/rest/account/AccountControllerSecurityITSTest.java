package com.itreflection.invoicer.integration.test.rest.account;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.itreflection.invoicer.integration.impl.rule.Login;
import com.itreflection.invoicer.integration.test.AbstractIntegrationTest;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.rest.api.controllers.AccountController;
import com.itreflection.invoicer.rest.api.model.ResponseDto;

public class AccountControllerSecurityITSTest extends AbstractIntegrationTest {

  public static final String USERNAME = "test_user";
  public static final String EMAIL = "test_email@test.com";
  public static final String PASSWORD = "password";

  @Autowired
  private AccountController accountController;

  @Autowired
  private AccountRepositoryService repositoryService;

  @Login(username = "a", password = "b", email = "c@c.com", authorities = { AuthorityType.TEST_AUTHORITY },
         create = false)
  @Test
  public void shouldViolateSecurity() throws Exception {
    //given
    AccountDto accountDto = repositoryService.findByName("a");

    //when
    try {
      accountController.save(accountDto);
      fail("User shouldn't be authorized to invoke this method");
    } catch (Exception e) {
      //then
      assertThat(e).isInstanceOf(AuthenticationCredentialsNotFoundException.class);
    }
  }

  @Login(username = "username", password = "password", email = "test_mail@test.com",
         authorities = { AuthorityType.TEST_AUTHORITY })
  @Test
  public void shouldViolateNotCorrectAuthority() throws Exception {
    //given
    AccountDto accountDto = repositoryService.findByName("username");

    //when
    try {
      accountController.save(accountDto);
      fail("User shouldn't be authorized to invoke this method");
    } catch (Exception e) {
      //then
      assertThat(e).isInstanceOf(AccessDeniedException.class);
    }
  }

  @Login(username = "username1", password = "password", email = "test1_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldNotViolate() throws Exception {
    //given
    AccountDto accountDto = repositoryService.findByName("username1");

    //when
    ResponseDto<AccountDto> response = accountController.save(accountDto);

    //then
    assertThat(response.getObject().getId()).isNotNull();
  }

  @Login(username = "username2", password = "password", email = "test2_mail@test.com",
         authorities = { AuthorityType.ADMIN, AuthorityType.ADMIN_DELETE_ACCOUNT })
  @Test
  public void shouldNotViolateAccessDeniedOtherIdForDeleteMethod() throws Exception {
    // User from Login annotation have ID 2
    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    Long userId = accountController.findByUserName(user.getUsername()).getObject().getId();

    accountController.delete(userId);
    //then
  }

  @Login(username = "username3", password = "password", email = "test3_mail@test.com",
         authorities = { AuthorityType.ADMIN })
  @Test
  public void shouldViolateAccessDeniedOtherIdForDeleteMethod() throws Exception {

    try {
      accountController.delete(1111L);
    } catch (Exception e) {
      assertThat(e).isInstanceOf(AccessDeniedException.class);
    }

  }

  private AccountDto createAccountDto() {
    AccountDto accountDto = new AccountDto();
    accountDto.setUsername(USERNAME);
    accountDto.setEmail(EMAIL);
    accountDto.setPassword(PASSWORD);
    return accountDto;
  }

}
