package com.itreflection.invoicer.integration.test;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.itreflection.invoicer.integration.configuration.IntegrationTestConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationTestConfiguration.class)
public class AbstractIntegrationTest {

  @Rule
  @Autowired
  public RuleChain integrationRuleChain;

  @Autowired
  ApplicationContext applicationContext;

  @Test
  public void test() {
    assertThat(applicationContext.getBean("accountConverterImpl")).isNotNull();
  }

}
