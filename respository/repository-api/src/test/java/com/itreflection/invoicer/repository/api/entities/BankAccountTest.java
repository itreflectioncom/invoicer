package com.itreflection.invoicer.repository.api.entities;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by sscode on 2017-02-04.
 */
public class BankAccountTest {

  public static final String IBAN = "AA00AA000000000000000";
  public static final String BIC = "AAaaAA00AA0";
  public static final String BANK_NAME = "Bank-Bank. Aaaa00";
  private static Validator validator;

  @BeforeClass
  public static void setup() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  private BankAccount createBankAccount() {
    BankAccount bankAccount = new BankAccount();
    bankAccount.setIban(IBAN);
    bankAccount.setBic(BIC);
    bankAccount.setBankName(BANK_NAME);
    return bankAccount;
  }

  @Test
  public void shouldNotHaveViolations() {
    BankAccount bankAccount = createBankAccount();
    Set<ConstraintViolation<BankAccount>> violations = validator.validate(bankAccount);
    assertThat(violations).isEmpty();
  }

  @Test
  public void shouldViolateBankNameSizeMin() {
    BankAccount bankAccount = createBankAccount();

    bankAccount.setBankName("A0");
    Set<ConstraintViolation<BankAccount>> violations = validator.validate(bankAccount);

    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Bank name should have size between 3 and 50");
  }

  @Test
  public void shouldViolateBankNameSizeMax() {
    BankAccount bankAccount = createBankAccount();

    bankAccount.setBankName("AA00000000000000000000000000000000000000000000000000");
    Set<ConstraintViolation<BankAccount>> violations = validator.validate(bankAccount);

    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Bank name should have size between 3 and 50");
  }

  @Test
  public void shouldViolateIbanNotValidChars() {
    BankAccount bankAccount = createBankAccount();

    bankAccount.setIban("@ .,+-{:'!$>/:%^&*[");
    Set<ConstraintViolation<BankAccount>> violations = validator.validate(bankAccount);

    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("IBAN number must match \"^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}$\"");
  }

  @Test
  public void shouldViolateBicNotValidChars() {
    BankAccount bankAccount = createBankAccount();

    bankAccount.setBic("@ .,>/^[$%");
    Set<ConstraintViolation<BankAccount>> violations = validator.validate(bankAccount);

    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("BIC number must match \"^([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)$\"");
  }

  @Test
  public void shouldViolateBankNameNotValidChars() {
    BankAccount bankAccount = createBankAccount();

    bankAccount.setBankName("@.,>/^[$%");
    Set<ConstraintViolation<BankAccount>> violations = validator.validate(bankAccount);

    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Bank name must match \"^[ A-Za-z0-9+_.-]+\"");
  }
}
