package com.itreflection.invoicer.repository.api.entities;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

public class ProductTest {

   private static final String NAME = "TestName";
   private static final String CODE = "TestCode";
   private static final String DESCRIPTION = "TestDescription";
   private static final Integer DISCOUNT = 50;
   private static final Integer TAX = 50;
   private static final String QUANTITYUNIT = "TestQUnit";
   private static Validator validator;

   @BeforeClass
   public static void setup() {
      ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
      validator = factory.getValidator();
   }

   @Test
   public void shouldNotHaveViolations() throws Exception {
      //given
      Product product = new Product();
      product.setName(NAME);
      product.setCode(CODE);
      product.setDescription(DESCRIPTION);
      product.setQuantityUnit(QUANTITYUNIT);
       product.setDefaultTaxes(TAX);
       product.setDefaultDiscount(DISCOUNT);

      //when
      Set<ConstraintViolation<Product>> violations = validator.validate(product);

      //then
      assertThat(violations).isEmpty();
   }

   @Test
   public void shouldViolateNameSizeMin() throws Exception {
      //given
      Product product = new Product();
      product.setName("a");
      product.setCode(CODE);
      product.setDescription(DESCRIPTION);
      product.setQuantityUnit(QUANTITYUNIT);

      //when
      Set<ConstraintViolation<Product>> violations = validator.validate(product);

      //then
      assertThat(violations).hasSize(1);
      assertThat(violations.iterator().next().getMessage()).isEqualTo("Product name should have size between 3 and 30");
   }

   @Test
   public void shouldViolateNameSizeMax() throws Exception {
      //given
      Product product = new Product();
      product.setName("a123456789012345678901234567890");
      product.setCode(CODE);
      product.setDescription(DESCRIPTION);
       product.setQuantityUnit(QUANTITYUNIT);

      //when
      Set<ConstraintViolation<Product>> violations = validator.validate(product);

      //then
      assertThat(violations).hasSize(1);
      assertThat(violations.iterator().next().getMessage()).isEqualTo("Product name should have size between 3 and 30");
   }

   @Test
   public void shouldViolateCodeMaxSize() throws Exception {
      //given
      Product product = new Product();
      product.setName(NAME);
      product.setCode("a123456789012345678901234567890");
      product.setDescription(DESCRIPTION);
       product.setQuantityUnit(QUANTITYUNIT);

      //when
      Set<ConstraintViolation<Product>> violations = validator.validate(product);

      //then
      assertThat(violations).hasSize(1);
      assertThat(violations.iterator().next().getMessage()).isEqualTo("Code should have size up to 30");
   }

   @Test
   public void shouldViolateDescriptionMaxSize() throws Exception {
      //given
      Product product = new Product();
      product.setName(NAME);
      product.setCode(CODE);
      product.setDescription("a123456789012345678901234567890a123456789012345678901234567890a123456789012345678901234567890" +
              "a123456789012345678901234567890");
       product.setQuantityUnit(QUANTITYUNIT);

      //when
      Set<ConstraintViolation<Product>> violations = validator.validate(product);

      //then
      assertThat(violations).hasSize(1);
      assertThat(violations.iterator().next().getMessage()).isEqualTo("Description should have up to 120");
   }
    @Test
    public void shouldViolateQuantityUnitSizeMin() throws Exception {
        //given
        Product product = new Product();
        product.setName(NAME);
        product.setCode(CODE);
        product.setDescription(DESCRIPTION);
        product.setDefaultDiscount(DISCOUNT);
        product.setQuantityUnit("");

        //when
        Set<ConstraintViolation<Product>> violations = validator.validate(product);

        //then
        assertThat(violations).hasSize(1);
        assertThat(violations.iterator().next().getMessage()).isEqualTo("Quantity Unit should have size between 1 and 10");
    }

    @Test
    public void shouldViolateQuantityUnitSizeMax() throws Exception {
        //given
        Product product = new Product();
        product.setName(NAME);
        product.setCode(CODE);
        product.setDescription(DESCRIPTION);
        product.setQuantityUnit("a1234567890");

        //when
        Set<ConstraintViolation<Product>> violations = validator.validate(product);

        //then
        assertThat(violations).hasSize(1);
        assertThat(violations.iterator().next().getMessage()).isEqualTo("Quantity Unit should have size between 1 and 10");
    }

    @Test
    public void shouldViolateDiscountValueMin() throws Exception {
        //given
        Product product = new Product();
        product.setName(NAME);
        product.setCode(CODE);
        product.setDescription(DESCRIPTION);
        product.setQuantityUnit(QUANTITYUNIT);
        product.setDefaultDiscount(-1);

        //when
        Set<ConstraintViolation<Product>> violations = validator.validate(product);

        //then
        assertThat(violations).hasSize(1);
        assertThat(violations.iterator().next().getMessage()).isEqualTo("Discount should be equal or grater than 0");
    }
    @Test
    public void shouldViolateDiscountValueMax() throws Exception {
        //given
        Product product = new Product();
        product.setName(NAME);
        product.setCode(CODE);
        product.setDescription(DESCRIPTION);
        product.setQuantityUnit(QUANTITYUNIT);
        product.setDefaultDiscount(101);

        //when
        Set<ConstraintViolation<Product>> violations = validator.validate(product);

        //then
        assertThat(violations).hasSize(1);
        assertThat(violations.iterator().next().getMessage()).isEqualTo("Discount should be less or equal to 100");
    }
    @Test
    public void shouldViolateTaxValueMin() throws Exception {
        //given
        Product product = new Product();
        product.setName(NAME);
        product.setCode(CODE);
        product.setDescription(DESCRIPTION);
        product.setQuantityUnit(QUANTITYUNIT);
        product.setDefaultTaxes(-1);

        //when
        Set<ConstraintViolation<Product>> violations = validator.validate(product);

        //then
        assertThat(violations).hasSize(1);
        assertThat(violations.iterator().next().getMessage()).isEqualTo("Tax should be equal or grater than 0");
    }
    @Test
    public void shouldViolateTaxValueMax() throws Exception {
        //given
        Product product = new Product();
        product.setName(NAME);
        product.setCode(CODE);
        product.setDescription(DESCRIPTION);
        product.setQuantityUnit(QUANTITYUNIT);
        product.setDefaultTaxes(101);

        //when
        Set<ConstraintViolation<Product>> violations = validator.validate(product);

        //then
        assertThat(violations).hasSize(1);
        assertThat(violations.iterator().next().getMessage()).isEqualTo("Tax should be less or equal to 100");
    }
}
