package com.itreflection.invoicer.repository.api.entities;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

public class AccountTest {

  public static final String USERNAME = "Username0_.-+";
  public static final String PASSWORD = "pass";
  public static final String EMAIL = "test@email.com";
  private static Validator validator;

  @BeforeClass
  public static void setup() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void shouldNotHaveViolations() throws Exception {
    //given
    Account account = new Account();
    account.setUsername(USERNAME);
    account.setPassword(PASSWORD);
    account.setEmail(EMAIL);
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).isEmpty();
  }

  @Test
  public void shouldViolateUsernameSizeMin() throws Exception {
    //given
    Account account = new Account();
    account.setUsername("a");
    account.setPassword(PASSWORD);
    account.setEmail(EMAIL);
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Username should have size between 3 and 30");
  }

  @Test
  public void shouldViolateUsernameSizeMax() throws Exception {
    //given
    Account account = new Account();
    account.setUsername("1234567890123456789012345678901");
    account.setPassword(PASSWORD);
    account.setEmail(EMAIL);
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Username should have size between 3 and 30");
  }

  @Test
  public void shouldViolateUsernameNotValidChars() throws Exception {
    //given
    Account account = new Account();
    account.setUsername("%Q@#$%&^");
    account.setPassword(PASSWORD);
    account.setEmail(EMAIL);
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Username must match \"^[A-Za-z0-9+_.-]+\"");
  }

  @Test
  public void shouldViolateEmailNotValidChars() throws Exception {
    //given
    Account account = new Account();
    account.setUsername(USERNAME);
    account.setPassword(PASSWORD);
    account.setEmail("%Q@#$%&^asdfasd");
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Email must match \"^[A-Za-z0-9+_.-]+@(.+)$\"");
  }

  @Test
  public void shouldViolateEmailMinSize() throws Exception {
    //given
    Account account = new Account();
    account.setUsername(USERNAME);
    account.setPassword(PASSWORD);
    account.setEmail("a@a.d");
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Email should have size between 10 and 30");
  }

  @Test
  public void shouldViolateEmailMaxSize() throws Exception {
    //given
    Account account = new Account();
    account.setUsername(USERNAME);
    account.setPassword(PASSWORD);
    account.setEmail("123456789012345678901234567890@1234567890.com");
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Email should have size between 10 and 30");
  }

  @Test
  public void shouldViolatePasswordMinSize() throws Exception {
    //given
    Account account = new Account();
    account.setUsername(USERNAME);
    account.setPassword("Aa");
    account.setEmail(EMAIL);
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Password should have size between 3 and 30");

  }

  @Test
  public void shouldViolatePasswordMaxSize() throws Exception {
    //given
    Account account = new Account();
    account.setUsername(USERNAME);
    account.setPassword("Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    account.setEmail(EMAIL);
    //      account.setAuthorityType(AuthorityType.USER);

    //when
    Set<ConstraintViolation<Account>> violations = validator.validate(account);

    //then
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Password should have size between 3 and 30");

  }

}