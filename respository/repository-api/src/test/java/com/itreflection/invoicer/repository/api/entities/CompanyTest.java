package com.itreflection.invoicer.repository.api.entities;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by sscode on 2017-01-30.
 */
public class CompanyTest {

  public static final String NAME = "Qwerty+_0986,./";
  public static final String TAX_ID = "123456789";
  public static final String UE_TAX_ID = "AA11234567";
  public static final String PHONE_NUMBER1 = "123456789";
  public static final String PHONE_NUMBER2 = "123456789";
  public static final String FAX = "123456789";
  public static final String EMAIL = "test@test.com";
  public static final String WEBSITE_URL = "www.test.com";
  private static Validator validator;

  @BeforeClass
  public static void setup() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  private Company createCompany() {
    Company company = new Company();
    company.setName(NAME);
    company.setTaxId(TAX_ID);
    company.setUeTaxId(UE_TAX_ID);
    company.setPhoneNumber1(PHONE_NUMBER1);
    company.setPhoneNumber2(PHONE_NUMBER2);
    company.setFax(FAX);
    company.setEmail(EMAIL);
    company.setWebsiteUrl(WEBSITE_URL);
    return company;
  }

  @Test
  public void shouldNotHaveViolations() throws Exception {
    Company company = createCompany();
    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).isEmpty();
  }

  @Test
  public void shouldViolateNameSizeMin() throws Exception {
    Company company = createCompany();
    company.setName("a");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);

    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Name length should have been longer or equal 3");
  }

  @Test
  public void shouldViolateTaxIdSizeMin() throws Exception {
    Company company = createCompany();
    company.setTaxId("123");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage())
        .isEqualTo("Tax identification should have size between 5 and 20");
  }

  @Test
  public void shouldViolateTaxIdSizeMax() throws Exception {
    Company company = createCompany();
    company.setTaxId("123456789012345678901");


    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage())
        .isEqualTo("Tax identification should have size between 5 and 20");
  }

  @Test
  public void shouldViolateTaxIdNotValidChars() throws Exception {
    Company company = createCompany();
    company.setTaxId("12334abc=-12345");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Tax identification must match \"^[0-9]+\"");
  }

  @Test
  public void shouldViolateUeTaxIdSizeMin() throws Exception {
    Company company = createCompany();
    company.setUeTaxId("AA23");


    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage())
        .isEqualTo("Tax identification should have size between 5 and 20");
  }

  @Test
  public void shouldViolateUeTaxIdSizeMax() throws Exception {
    Company company = createCompany();
    company.setUeTaxId("AA3456789012345678901");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage())
        .isEqualTo("Tax identification should have size between 5 and 20");
  }

  @Test
  public void shouldViolateUeTaxIdNotValidChars() throws Exception {
    Company company = createCompany();
    company.setUeTaxId("12334abc=-12345");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Tax identification must match \"^[A-Z]{2}+[0-9]+\"");
  }

  @Test
  public void shouldViolatePhone1SizeMin() throws Exception {
    Company company = createCompany();
    company.setPhoneNumber1("1");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Phone number should have size between 6 and 12");
  }

  @Test
  public void shouldViolatePhone1SizeMax() throws Exception {
    Company company = createCompany();
    company.setPhoneNumber1("1234567890123");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Phone number should have size between 6 and 12");
  }

  @Test
  public void shouldViolatePhone1NotValidChars() throws Exception {
    Company company = createCompany();
    company.setPhoneNumber1("1234a+-12");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Phone number must match \"^[0-9]+\"");
  }

  @Test
  public void shouldViolatePhone2SizeMin() throws Exception {
    Company company = createCompany();
    company.setPhoneNumber2("1");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Phone number should have size between 6 and 12");
  }

  @Test
  public void shouldViolatePhone2SizeMax() throws Exception {
    Company company = createCompany();
    company.setPhoneNumber2("1234567890123");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Phone number should have size between 6 and 12");
  }

  @Test
  public void shouldViolatePhone2NotValidChars() throws Exception {
    Company company = createCompany();
    company.setPhoneNumber2("1234a+-12");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Phone number must match \"^[0-9]+\"");
  }

  @Test
  public void shouldViolateFaxSizeMin() throws Exception {
    Company company = createCompany();
    company.setFax("1");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Fax number should have size between 6 and 12");
  }

  @Test
  public void shouldViolateFaxSizeMax() throws Exception {
    Company company = createCompany();
    company.setFax("1234567890123");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Fax number should have size between 6 and 12");
  }

  @Test
  public void shouldViolateFaxNotValidChars() throws Exception {
    Company company = createCompany();
    company.setFax("1234a+-12");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Fax number must match \"^[0-9]+\"");
  }

  @Test
  public void shouldViolateEmailSizeMin() throws Exception {
    Company company = createCompany();
    company.setEmail("a@a.a");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Email should have size between 10 and 30");
  }

  @Test
  public void shouldViolateEmailSizeMax() throws Exception {
    Company company = createCompany();
    company.setEmail("aaaaaaaaaaaaaa122345667890aaaaaaaaaaaa@aaaaaa1234567890.com");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Email should have size between 10 and 30");
  }

  @Test
  public void shouldViolateEmailNotValidChars() throws Exception {
    Company company = createCompany();
    company.setEmail("1234a+-121234");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Email must match \"^[A-Za-z0-9+_.-]+@(.+)$\"");
  }

  @Test
  public void shouldViolateWebsiteUrlSizeMin() throws Exception {
    Company company = createCompany();
    company.setWebsiteUrl("a.a.a");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Website address should have size between 7 and 40");
  }

  @Test
  public void shouldViolateWebsiteUrlSizeMax() throws Exception {
    Company company = createCompany();
    company.setWebsiteUrl("www.aaaaaaaaaaaaaa1223456167890aaaaaaaaaaaaaaaaaa1234567890.com");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage())
        .isEqualTo("Website address should have size between 7 and 40");
  }

  @Test
  public void shouldViolateWebsiteUrlNotValidChars() throws Exception {
    Company company = createCompany();
    company.setWebsiteUrl("aaaaaaaaa");

    Set<ConstraintViolation<Company>> violations = validator.validate(company);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Invalid Website address eg. 'www.test.com'");
  }

}
