package com.itreflection.invoicer.repository.api.entities;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by sscode on 2017-01-31.
 */
public class AddressTest {
  public static final String COUNTRY = "Abcde";
  public static final String CITY = "Abcd-Ab cd1";
  public static final String STREET_NAME = "Abcd-ab cd1";
  public static final String HOUSE_NUMBER = "1000a";
  public static final String APART_NUMBER = "1000a";
  public static final String ZIP_CODE = "10-000";
  private static Validator validator;

  @BeforeClass
  public static void setup() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  private Address createAddress() {
    Address address = new Address();
    address.setCountry(COUNTRY);
    address.setCity(CITY);
    address.setStreetName(STREET_NAME);
    address.setHouseNumber(HOUSE_NUMBER);
    address.setApartNumber(APART_NUMBER);
    address.setZipCode(ZIP_CODE);
    return address;
  }

  @Test
  public void shouldNotHaveViolations() throws Exception {
    Address address = createAddress();

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).isEmpty();
  }

  @Test
  public void shouldViolateCountryMinSize() {
    Address address = createAddress();
    address.setCountry("Aa");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Country name should have size between 3 and 20");
  }

  @Test
  public void shouldViolateCountryMaxSize() {
    Address address = createAddress();
    address.setCountry("Aaaaaaaaaaaaaaaaaaaaaaabbbbbbb");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Country name should have size between 3 and 20");
  }

  @Test
  public void shouldViolateCountryNotValidChars() {
    Address address = createAddress();
    address.setCountry("%Q@#$%&^0");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Country name must match \"^[A-Za-z]$\"");
  }

  @Test
  public void shouldViolateCityMinSize() {
    Address address = createAddress();
    address.setCity("A");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("City name should have size between 2 and 40");
  }

  @Test
  public void shouldViolateCityMaxSize() {
    Address address = createAddress();
    address.setCity("Aaaaaaaaaaaaaaaaaaaaaaabbbbbbbccccccccccccc");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("City name should have size between 2 and 40");
  }

  @Test
  public void shouldViolateCityNotValidChars() {
    Address address = createAddress();
    address.setCity("%Q@#$%&^");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("City name must match \"^[ 0-9A-Za-z-]$\"");
  }

  @Test
  public void shouldViolateStreetNameMinSize() {
    Address address = createAddress();
    address.setStreetName("A");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Street name should have size between 2 and 40");
  }

  @Test
  public void shouldViolateStreetNameMaxSize() {
    Address address = createAddress();
    address.setStreetName("Aaaaaaaaaaaaaaaaaaaaaaabbbbbbbccccccccccccc");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Street name should have size between 2 and 40");
  }

  @Test
  public void shouldViolateStreetNameNotValidChars() {
    Address address = createAddress();
    address.setStreetName("%Q@#$%&^");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Street name must match \"^[ 0-9A-Za-z-]$\"");
  }

  @Test
  public void shouldViolateHouseNumberMaxSize() {
    Address address = createAddress();
    address.setHouseNumber("1000000ccccccc");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("House number should have length lesser then 10");

  }

  @Test
  public void shouldViolateHouseNumberNotValidChars() {
    Address address = createAddress();
    address.setHouseNumber("%Q@#$%&^");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("House number must match \"^[0-9A-Za-z]$\"");
  }

  @Test
  public void shouldViolateApartNumberMaxSize() {
    Address address = createAddress();
    address.setApartNumber("1000000ccccccc");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Apartment number should have length lesser then 10");

  }

  @Test
  public void shouldViolateApartNumberNotValidChars() {
    Address address = createAddress();
    address.setApartNumber("%Q@#$%&^");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Apartment number must match \"^[0-9A-Za-z]$\"");
  }

  @Test
  public void shouldViolateZipCodeMinSize() {
    Address address = createAddress();
    address.setZipCode("00");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Zip code should have size between 4 and 9");
  }

  @Test
  public void shouldViolateZipCodeMaxSize() {
    Address address = createAddress();
    address.setZipCode("00-00000000");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Zip code should have size between 4 and 9");
  }

  @Test
  public void shouldViolateZipCodeNotValidChars() {
    Address address = createAddress();
    address.setZipCode("aaa@*&^");

    Set<ConstraintViolation<Address>> violations = validator.validate(address);
    assertThat(violations).hasSize(1);
    assertThat(violations.iterator().next().getMessage()).isEqualTo("Zip code must match \"^[0-9-]+\"");
  }





}
