package com.itreflection.invoicer.repository.api.entities;

public enum Type {
        PRODUCT,
        SERVICE
}
