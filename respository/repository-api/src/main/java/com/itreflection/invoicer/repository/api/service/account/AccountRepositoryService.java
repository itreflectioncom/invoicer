package com.itreflection.invoicer.repository.api.service.account;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.service.RepositoryService;

public interface AccountRepositoryService extends RepositoryService<AccountDto, Long> {

  AccountDto findByName(String name);
}
