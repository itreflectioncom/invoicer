package com.itreflection.invoicer.repository.api.entities;

/**
 * Created by sscode on 2017-03-31.
 */
public enum AuthorityType {
  VIEW_ACCOUNT,
  MANAGE_ACCOUNT,
  TEST_AUTHORITY,
  ADMIN,
  ADMIN_DELETE_ACCOUNT,
  ADMIN_UPDATE_ACCOUNT
}
