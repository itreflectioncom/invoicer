package com.itreflection.invoicer.repository.api.service;

import java.util.List;

public interface RepositoryService<V, K> {

  V save(V object);

  V getById(K id);

  List<V> findAll();

  void delete(K id);

}
