package com.itreflection.invoicer.repository.api.service.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.itreflection.invoicer.repository.api.dto.AddressDto;
import com.itreflection.invoicer.repository.api.service.RepositoryService;

/**
 * Created by sscode on 2017-02-06.
 */
public interface AddressRepositoryService extends RepositoryService<AddressDto, Long> {

  @Query(value = "select a from COMPANY c join c.address a where c.name = :companyName")
  AddressDto findByCompany(@Param("companyName") String name);
}
