package com.itreflection.invoicer.repository.api.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

@Entity(name = "ACCOUNT")
public class Account {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column(unique = true, length = 30, nullable = false)
  @Size(min = 3, max = 30, message = "Username should have size between 3 and 30")
  @Pattern(regexp = "^[A-Za-z0-9+_.-]+", message = "Username must match \"^[A-Za-z0-9+_.-]+\"")
  private String username;
  @Column(unique = true, length = 40, nullable = false)
  @Size(min = 10, max = 40, message = "Email should have size between 10 and 30")
  @Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Email must match \"^[A-Za-z0-9+_.-]+@(.+)$\"")
  private String email;
  @Column(length = 30)
  @Size(min = 3, max = 30, message = "Password should have size between 3 and 30")
  private String password;
  @OneToMany(mappedBy = "accountId", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  //  @Cascade(CascadeType.ALL)
  private List<Authority> authorities;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private DateTime created;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private DateTime updated;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private DateTime lastLoginTime;
  @Type(type = "true_false")
  private Boolean enabled;

  public Account() {
    enabled = Boolean.TRUE;
    authorities = new ArrayList<>();
  }

  @PrePersist
  protected void onCreate() {
    created = DateTime.now(DateTimeZone.UTC);
  }

  @PreUpdate
  protected void onUpdate() {
    updated = DateTime.now(DateTimeZone.UTC);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<Authority> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(List<Authority> authorities) {
    this.authorities = authorities;
  }

  public DateTime getCreated() {
    return created;
  }

  public void setCreated(DateTime created) {
    this.created = created;
  }

  public DateTime getUpdated() {
    return updated;
  }

  public void setUpdated(DateTime updated) {
    this.updated = updated;
  }

  public DateTime getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(DateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("username", username)
        .append("email", email).append("password", password).append("authorities", authorities)
        .append("created", created).append("updated", updated).append("lastLoginTime", lastLoginTime)
        .append("enabled", enabled).toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    Account rhs = (Account) obj;
    return new EqualsBuilder().append(this.id, rhs.id).append(this.username, rhs.username).append(this.email, rhs.email)
        .append(this.password, rhs.password).append(this.authorities, rhs.authorities).append(this.created, rhs.created)
        .append(this.updated, rhs.updated).append(this.lastLoginTime, rhs.lastLoginTime)
        .append(this.enabled, rhs.enabled).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(username).append(email).append(password).append(authorities)
        .append(created).append(updated).append(lastLoginTime).append(enabled).toHashCode();
  }
}
