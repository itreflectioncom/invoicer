package com.itreflection.invoicer.repository.api.service.company;

import com.itreflection.invoicer.repository.api.dto.CompanyDto;
import com.itreflection.invoicer.repository.api.service.RepositoryService;

/**
 * Created by sscode on 2017-02-05.
 */
public interface CompanyRepositoryService extends RepositoryService<CompanyDto, Long> {
  CompanyDto findByName(String name);
  CompanyDto findByTaxId(String taxId);
  CompanyDto findByEmail(String email);
}
