package com.itreflection.invoicer.repository.api.dto;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by sscode on 2017-02-06.
 */
public class BankAccountDto {

  private Long id;
  private String iban;
  private String bic;
  private String bankName;
  private Date creationTime;
  private Date lastChangeTime;

  public BankAccountDto() {
  }

  public BankAccountDto(String iban, String bic, String bankName, Date creationTime, Date lastChangeTime) {
    this.iban = iban;
    this.bic = bic;
    this.bankName = bankName;
    this.creationTime = creationTime;
    this.lastChangeTime = lastChangeTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public String getBic() {
    return bic;
  }

  public void setBic(String bic) {
    this.bic = bic;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public Date getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(Date creationTime) {
    this.creationTime = creationTime;
  }

  public Date getLastChangeTime() {
    return lastChangeTime;
  }

  public void setLastChangeTime(Date lastChangeTime) {
    this.lastChangeTime = lastChangeTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;

    if (o == null)
      return false;

    if (o.getClass() != getClass())
      return false;

    BankAccountDto that = (BankAccountDto) o;

    return new EqualsBuilder().append(id, that.id).append(iban, that.iban).append(bic, that.bic)
        .append(bankName, that.bankName).append(creationTime, that.creationTime)
        .append(lastChangeTime, that.lastChangeTime).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(iban).append(bic).append(bankName).append(creationTime)
        .append(lastChangeTime).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("iban", iban).append("bic", bic)
        .append("bankName", bankName).append("creationTime", creationTime).append("lastChangeTime", lastChangeTime)
        .toString();
  }
}
