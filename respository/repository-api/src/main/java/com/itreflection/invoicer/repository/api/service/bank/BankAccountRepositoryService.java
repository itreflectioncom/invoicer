package com.itreflection.invoicer.repository.api.service.bank;

import com.itreflection.invoicer.repository.api.dto.BankAccountDto;
import com.itreflection.invoicer.repository.api.service.RepositoryService;

/**
 * Created by sscode on 2017-02-06.
 */
public interface BankAccountRepositoryService extends RepositoryService<BankAccountDto, Long> {
  BankAccountDto findByIban(String iban);
}
