package com.itreflection.invoicer.repository.api.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.itreflection.invoicer.repository.api.entities.AuthorityType;

public class AccountDto {

  private Long id;
  private String username;
  private String email;
  private String password;
  private List<AuthorityType> authorities;
  private DateTime created;
  private DateTime updated;
  private DateTime lastLoginTime;

  public AccountDto() {
    this.authorities = new ArrayList<>();
  }

  public AccountDto(String username, String email, String password) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.authorities = new ArrayList<>();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public DateTime getCreated() {
    return created;
  }

  public void setCreated(DateTime created) {
    this.created = created;
  }

  public DateTime getUpdated() {
    return updated;
  }

  public void setUpdated(DateTime updated) {
    this.updated = updated;
  }

  public DateTime getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(DateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public List<AuthorityType> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(List<AuthorityType> authorities) {
    this.authorities = authorities;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("username", username)
        .append("email", email).append("password", password).append("authorities", authorities)
        .append("created", created).append("updated", updated).append("lastLoginTime", lastLoginTime).toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    AccountDto rhs = (AccountDto) obj;
    return new EqualsBuilder().append(this.id, rhs.id).append(this.username, rhs.username).append(this.email, rhs.email)
        .append(this.password, rhs.password).append(this.authorities, rhs.authorities).append(this.created, rhs.created)
        .append(this.updated, rhs.updated).append(this.lastLoginTime, rhs.lastLoginTime).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(username).append(email).append(password).append(authorities)
        .append(created).append(updated).append(lastLoginTime).toHashCode();
  }
}
