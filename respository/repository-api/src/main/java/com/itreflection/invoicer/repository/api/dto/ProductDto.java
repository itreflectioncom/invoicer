package com.itreflection.invoicer.repository.api.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ProductDto {

    private Long id;
    private Enum type;
    private Enum status;
    private String name;
    private String code;
    private String description;
    private Double defaultPrice;
    private Integer defaultTax;
    private Integer defaultDiscount;
    private Double quantity;
    private String quantityUnit;

    public ProductDto() {

    }

    public ProductDto(Long id, Enum type, Enum status, String name, String code, String description,
                      Double defaultPrice, Integer defaultTax, Integer defaultDiscount, Double quantity,
                      String quantityUnit) {
        this.id = id;
        this.type = type;
        this.status = status;
        this.name = name;
        this.code = code;
        this.description = description;
        this.defaultPrice = defaultPrice;
        this.defaultTax = defaultTax;
        this.defaultDiscount = defaultDiscount;
        this.quantity = quantity;
        this.quantityUnit = quantityUnit;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Enum getType() {
        return type;
    }

    public void setType(Enum type) {
        this.type = type;
    }

    public Enum getStatus() {
        return status;
    }

    public void setStatus(Enum status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(Double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public int getDefaultTaxes() {
        return defaultTax;
    }

    public void setDefaultTaxes(int defaultTaxes) {
        this.defaultTax = defaultTaxes;
    }

    public Integer getDefaultDiscount() {
        return defaultDiscount;
    }

    public void setDefaultDiscount(Integer defaultDiscount) {
        this.defaultDiscount = defaultDiscount;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getQuantityUnit() {
        return quantityUnit;
    }

    public void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }




    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ProductDto prd = (ProductDto) obj;
        return new EqualsBuilder().append(this.id, prd.id).append(this.type, prd.type).append(this.status, prd.status)
                .append(this.name, prd.name).append(this.code, prd.code)
                .append(this.description, prd.description).append(this.defaultPrice, prd.defaultPrice)
                .append(this.defaultTax, prd.defaultTax).append(this.defaultDiscount, prd.defaultDiscount)
                .append(this.quantity, prd.quantity).append(this.quantityUnit, prd.quantityUnit).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(type).append(status).append(name).append(code).append(description)
                .append(defaultPrice).append(defaultTax).append(defaultDiscount).append(quantity).append(quantityUnit)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("type", type).append("status", status)
                .append("name", name).append("code", code).append("description", description).append("defaultPrice", defaultPrice)
                .append("defaultTax", defaultTax).append("defaultDiscount", defaultDiscount).append("quantity", quantity)
                .append("quantityUnit", quantityUnit).toString();
    }
}
