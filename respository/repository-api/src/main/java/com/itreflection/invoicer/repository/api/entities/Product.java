package com.itreflection.invoicer.repository.api.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(EnumType.STRING)
    private Enum type;
    @Enumerated(EnumType.STRING)
    private Enum status;
    @Column(unique = true, length = 30, nullable = false)
    @Size(min = 3, max = 30, message = "Product name should have size between 3 and 30")
    private String name;
    @Column(length = 30, nullable = false)
    @Size(max = 30, message = "Code should have size up to 30")
    private String code;
    @Column(length = 120)
    @Size(max = 120, message = "Description should have up to 120")
    private String description;
    @Column(precision = 10, scale = 2)
    private Double defaultPrice;
    @Column
    @Min(value = 0, message = "Tax should be equal or grater than 0")
    @Max(value = 100, message = "Tax should be less or equal to 100")
    private Integer defaultTax;
    @Column
    @Min(value = 0, message = "Discount should be equal or grater than 0")
    @Max(value = 100, message = "Discount should be less or equal to 100")
    private Integer defaultDiscount;
    @Column(precision = 10, scale = 2)
    private Double quantity;
    @Column(length = 10, nullable = false)
    @Size(min = 1, max = 10, message = "Quantity Unit should have size between 1 and 10")
    private String quantityUnit;
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    public Product() {

    }

    public Product(Long id, Enum type, Enum status, String name, String code, String description,
                   Double defaultPrice, Integer defaultTax, Integer defaultDiscount, Double quantity,
                   String quantityUnit, Account account) {
        this.id = id;
        this.type = type;
        this.status = status;
        this.name = name;
        this.code = code;
        this.description = description;
        this.defaultPrice = defaultPrice;
        this.defaultTax = defaultTax;
        this.defaultDiscount = defaultDiscount;
        this.quantity = quantity;
        this.quantityUnit = quantityUnit;
        this.account = account;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Enum getType() {
        return type;
    }

    public void setType(Enum type) {
        this.type = type;
    }

    public Enum getStatus() {
        return status;
    }

    public void setStatus(Enum status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(Double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public int getDefaultTaxes() {
        return defaultTax;
    }

    public void setDefaultTaxes(int defaultTaxes) {
        this.defaultTax = defaultTaxes;
    }

    public Integer getDefaultDiscount() {
        return defaultDiscount;
    }

    public void setDefaultDiscount(Integer defaultDiscount) {
        this.defaultDiscount = defaultDiscount;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getQuantityUnit() {
        return quantityUnit;
    }

    public void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    public Account getAccount_id() {
        return account;
    }

    public void setAccount_id(Account account_id) {
        this.account = account_id;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Product prd = (Product) obj;
        return new EqualsBuilder().append(this.id, prd.id).append(this.type, prd.type).append(this.status, prd.status)
                .append(this.name, prd.name).append(this.code, prd.code)
                .append(this.description, prd.description).append(this.defaultPrice, prd.defaultPrice)
                .append(this.defaultTax, prd.defaultTax).append(this.defaultDiscount, prd.defaultDiscount)
                .append(this.quantity, prd.quantity).append(this.quantityUnit, prd.quantityUnit)
                .append(this.account, prd.account).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(type).append(status).append(name).append(code).append(description)
                .append(defaultPrice).append(defaultTax).append(defaultDiscount).append(quantity).append(quantityUnit)
                .append(account).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("type", type).append("status", status)
                .append("name", name).append("code", code).append("description", description).append("defaultPrice", defaultPrice)
                .append("defaultTax", defaultTax).append("defaultDiscount", defaultDiscount).append("quantity", quantity)
                .append("quantityUnit", quantityUnit).append("account", account).toString();
    }
}
