package com.itreflection.invoicer.repository.api.entities;

public enum Status {
    PREDEFINED,
    FINAL
}

