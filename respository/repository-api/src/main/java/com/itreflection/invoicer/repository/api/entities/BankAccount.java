package com.itreflection.invoicer.repository.api.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;

/**
 * Created by sscode on 2017-01-28.
 */
@Entity(name = "BANK_ACCOUNT")
public class BankAccount {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Size(min = 16, max = 30, message = "IBAN number should have size between 16 and 30")
  @Pattern(regexp = "^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}$", message = "IBAN number must match \"^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}$\"")
  private String iban;
  @Size(min = 8, max = 11, message = "BIC number should have size between 8 and 11")
  @Pattern(regexp = "^([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)$", message = "BIC number must match \"^([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)$\"")
  private String bic;
  @Size(min = 3, max = 50, message = "Bank name should have size between 3 and 50")
  @Pattern(regexp = "^[ A-Za-z0-9+_.-]+", message = "Bank name must match \"^[ A-Za-z0-9+_.-]+\"")
  private String bankName;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private Date creationTime;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private Date lastChangeTime;

  public BankAccount() {
  }

  public BankAccount(String iban, String bic, String bankName, Date creationTime, Date lastChangeTime) {
    this.iban = iban;
    this.bic = bic;
    this.bankName = bankName;
    this.creationTime = creationTime;
    this.lastChangeTime = lastChangeTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public String getBic() {
    return bic;
  }

  public void setBic(String bic) {
    this.bic = bic;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public Date getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(Date creationTime) {
    this.creationTime = creationTime;
  }

  public Date getLastChangeTime() {
    return lastChangeTime;
  }

  public void setLastChangeTime(Date lastChangeTime) {
    this.lastChangeTime = lastChangeTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;

    if (o == null)
      return false;

    if (o.getClass() != getClass())
      return false;

    BankAccount that = (BankAccount) o;

    return new EqualsBuilder().append(id, that.id).append(iban, that.iban).append(bic, that.bic)
        .append(bankName, that.bankName).append(creationTime, that.creationTime)
        .append(lastChangeTime, that.lastChangeTime).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(iban).append(bic).append(bankName).append(creationTime)
        .append(lastChangeTime).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("iban", iban).append("bic", bic)
        .append("bankName", bankName).append("creationTime", creationTime).append("lastChangeTime", lastChangeTime)
        .toString();
  }
}
