package com.itreflection.invoicer.repository.api.service.product;

import com.itreflection.invoicer.repository.api.dto.ProductDto;
import com.itreflection.invoicer.repository.api.service.RepositoryService;

public interface ProductRepositoryService extends RepositoryService<ProductDto, Long> {

  ProductDto findByName(String name);
}
