package com.itreflection.invoicer.repository.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Created by sscode on 2017-01-28.
 */
@Entity(name = "COMPANY")
public class Company {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @OneToOne
  @JoinColumn(name = "ACCOUNT_ID", nullable = false)
  private Account account;
  @Column(unique = true)
  @NotNull
  @Size(min = 3, message = "Name length should have been longer or equal 3")
  private String name;
  @Column(unique = true)
  @NotNull
  @Pattern(regexp = "^[0-9]+", message = "Tax identification must match \"^[0-9]+\"")
  @Size(min = 5, max = 20, message = "Tax identification should have size between 5 and 20")
  private String taxId;
  @Column(unique = true)
  @NotNull
  @Pattern(regexp = "^[A-Z]{2}+[0-9]+", message = "Tax identification must match \"^[A-Z]{2}+[0-9]+\"")
  @Size(min = 5, max = 20, message = "Tax identification should have size between 5 and 20")
  private String ueTaxId;
  @OneToOne
  @JoinColumn(name = "ADDRESS_ID", nullable = false)
  private Address address;
  @Pattern(regexp = "^[0-9]+", message = "Phone number must match \"^[0-9]+\"")
  @NotNull
  @Size(min = 6, max = 12, message = "Phone number should have size between 6 and 12")
  private String phoneNumber1;
  @Pattern(regexp = "^[0-9]+", message = "Phone number must match \"^[0-9]+\"")
  @Size(min = 6, max = 12, message = "Phone number should have size between 6 and 12")
  private String phoneNumber2;
  @Pattern(regexp = "^[0-9]+", message = "Fax number must match \"^[0-9]+\"")
  @Size(min = 6, max = 12, message = "Fax number should have size between 6 and 12")
  private String fax;
  @Column(unique = true, length = 40, nullable = false)
  @Size(min = 10, max = 40, message = "Email should have size between 10 and 30")
  @Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Email must match \"^[A-Za-z0-9+_.-]+@(.+)$\"")
  private String email;
  @Size(min = 7, max = 40, message = "Website address should have size between 7 and 40")
  @Pattern(regexp = "^[a-z0-9+_.-]+[.][a-z0-9+_.-]+$", message = "Invalid Website address eg. 'www.test.com'")
  private String websiteUrl;
  @OneToOne
  @JoinColumn
  private BankAccount backAccount;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private DateTime created;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private DateTime updated;

  public Company() {
  }

  public Company(Account account, String name, String taxId, String ueTaxId, Address address, String phoneNumber1,
      String phoneNumber2, String fax, String email, String websiteUrl, BankAccount backAccount, DateTime created,
      DateTime updated) {
    this.account = account;
    this.name = name;
    this.taxId = taxId;
    this.ueTaxId = ueTaxId;
    this.address = address;
    this.phoneNumber1 = phoneNumber1;
    this.phoneNumber2 = phoneNumber2;
    this.fax = fax;
    this.email = email;
    this.websiteUrl = websiteUrl;
    this.backAccount = backAccount;
    this.created = created;
    this.updated = updated;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Account getAccountId() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTaxId() {
    return taxId;
  }

  public void setTaxId(String taxId) {
    this.taxId = taxId;
  }

  public String getUeTaxId() {
    return ueTaxId;
  }

  public void setUeTaxId(String ueTaxId) {
    this.ueTaxId = ueTaxId;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getPhoneNumber1() {
    return phoneNumber1;
  }

  public void setPhoneNumber1(String phoneNumber1) {
    this.phoneNumber1 = phoneNumber1;
  }

  public String getPhoneNumber2() {
    return phoneNumber2;
  }

  public void setPhoneNumber2(String phoneNumber2) {
    this.phoneNumber2 = phoneNumber2;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getWebsiteUrl() {
    return websiteUrl;
  }

  public void setWebsiteUrl(String websiteUrl) {
    this.websiteUrl = websiteUrl;
  }

  public BankAccount getBackAccount() {
    return backAccount;
  }

  public void setBackAccount(BankAccount backAccount) {
    this.backAccount = backAccount;
  }

  public DateTime getCreated() {
    return created;
  }

  public void setCreated(DateTime created) {
    this.created = created;
  }

  public DateTime getUpdated() {
    return updated;
  }

  public void setUpdated(DateTime updated) {
    this.updated = updated;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;

    if (o == null)
      return false;

    if (o.getClass() != getClass())
      return false;

    Company company = (Company) o;

    return new EqualsBuilder().append(id, company.id).append(account, company.account).append(name, company.name)
        .append(taxId, company.taxId).append(ueTaxId, company.ueTaxId).append(address, company.address)
        .append(phoneNumber1, company.phoneNumber1).append(phoneNumber2, company.phoneNumber2).append(fax, company.fax)
        .append(email, company.email).append(websiteUrl, company.websiteUrl).append(backAccount, company.backAccount)
        .append(created, company.created).append(updated, company.updated).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(account).append(name).append(taxId).append(ueTaxId).append(address)
        .append(phoneNumber1).append(phoneNumber2).append(fax).append(email).append(websiteUrl).append(backAccount)
        .append(created).append(updated).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("account", account)
        .append("name", name).append("taxId", taxId).append("ueTaxId", ueTaxId).append("address", address)
        .append("phoneNumber1", phoneNumber1).append("phoneNumber2", phoneNumber2).append("fax", fax)
        .append("email", email).append("websiteUrl", websiteUrl).append("backAccount", backAccount)
        .append("created", created).append("updated", updated).toString();
  }

  @PrePersist
  protected void onCreate() {
    created = DateTime.now(DateTimeZone.UTC);
  }

  @PreUpdate
  protected void onUpdate() {
    updated = DateTime.now(DateTimeZone.UTC);
  }

}
