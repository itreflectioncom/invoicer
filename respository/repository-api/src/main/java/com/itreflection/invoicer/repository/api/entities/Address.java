package com.itreflection.invoicer.repository.api.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;

/**
 * Created by sscode on 2017-01-28.
 */
@Entity(name = "ADDRESS")
public class Address {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NotNull
  @Size(min = 3, max = 20, message = "Country name should have size between 3 and 20")
  @Pattern(regexp = "^[A-Za-z]+", message = "Country name must match \"^[A-Za-z]$\"")
  private String country;
  @NotNull
  @Size(min = 2, max = 40, message = "City name should have size between 2 and 40")
  @Pattern(regexp = "^[ 0-9A-Za-z+-]+", message = "City name must match \"^[ 0-9A-Za-z-]$\"")
  private String city;
  @NotNull
  @Size(min = 2, max = 40, message = "Street name should have size between 2 and 40")
  @Pattern(regexp = "^[ 0-9A-Za-z+-]+", message = "Street name must match \"^[ 0-9A-Za-z-]$\"")
  private String streetName;
  @NotNull
  @Size(max = 10, message = "House number should have length lesser then 10")
  @Pattern(regexp = "^[0-9A-Za-z]+", message = "House number must match \"^[0-9A-Za-z]$\"")
  private String houseNumber;
  @Size(max = 10, message = "Apartment number should have length lesser then 10")
  @Pattern(regexp = "^[0-9A-Za-z]+", message = "Apartment number must match \"^[0-9A-Za-z]$\"")
  private String apartNumber;
  @NotNull
  @Size(min = 4, max = 9, message = "Zip code should have size between 4 and 9")
  @Pattern(regexp = "^[0-9-]+", message = "Zip code must match \"^[0-9-]+\"")
  private String zipCode;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private Date creationTime;
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
  private Date lastChangeTime;

  public Address() {
  }

  public Address(String country, String city, String streetName, String houseNumber,
      String apartNumber, String zipCode, Date creationTime, Date lastChangeTime) {
    this.country = country;
    this.city = city;
    this.streetName = streetName;
    this.houseNumber = houseNumber;
    this.apartNumber = apartNumber;
    this.zipCode = zipCode;
    this.creationTime = creationTime;
    this.lastChangeTime = lastChangeTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  public String getApartNumber() {
    return apartNumber;
  }

  public void setApartNumber(String apartNumber) {
    this.apartNumber = apartNumber;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public Date getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(Date creationTime) {
    this.creationTime = creationTime;
  }

  public Date getLastChangeTime() {
    return lastChangeTime;
  }

  public void setLastChangeTime(Date lastChangeTime) {
    this.lastChangeTime = lastChangeTime;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;

    if (o == null)
      return false;

    if (o.getClass() != getClass())
      return false;

    Address address1 = (Address) o;

    return new EqualsBuilder().append(id, address1.id).append(country, address1.country)
        .append(city, address1.city).append(streetName, address1.streetName)
        .append(houseNumber, address1.houseNumber)
        .append(apartNumber, address1.apartNumber).append(zipCode, address1.zipCode)
        .append(creationTime, address1.creationTime).append(lastChangeTime, address1.lastChangeTime).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(country).append(city).append(streetName)
        .append(houseNumber).append(apartNumber).append(zipCode).append(creationTime)
        .append(lastChangeTime).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("country", country).append("city",
        city)
        .append("streetName", streetName).append("houseNumber", houseNumber)
        .append("apartNumber", apartNumber).append("zipCode", zipCode)
        .append("creationTime", creationTime).append("lastChangeTime", lastChangeTime).toString();
  }
}
