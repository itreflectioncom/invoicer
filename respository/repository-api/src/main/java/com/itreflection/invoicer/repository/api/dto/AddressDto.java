package com.itreflection.invoicer.repository.api.dto;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by sscode on 2017-02-06.
 */
public class AddressDto {

  private Long id;
  private String country;
  private String city;
  private String streetName;
  private String houseNumber;
  private String apartNumber;
  private String zipCode;
  private Date creationTime;
  private Date lastChangeTime;

  public AddressDto() {
  }

  public AddressDto(String country, String city, String streetName, String houseNumber,
      String apartNumber, String zipCode, Date creationTime, Date lastChangeTime) {
    this.country = country;
    this.city = city;
    this.streetName = streetName;
    this.houseNumber = houseNumber;
    this.apartNumber = apartNumber;
    this.zipCode = zipCode;
    this.creationTime = creationTime;
    this.lastChangeTime = lastChangeTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  public String getApartNumber() {
    return apartNumber;
  }

  public void setApartNumber(String apartNumber) {
    this.apartNumber = apartNumber;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public Date getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(Date creationTime) {
    this.creationTime = creationTime;
  }

  public Date getLastChangeTime() {
    return lastChangeTime;
  }

  public void setLastChangeTime(Date lastChangeTime) {
    this.lastChangeTime = lastChangeTime;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;

    if (o == null)
      return false;

    if (o.getClass() != getClass())
      return false;

    AddressDto address1 = (AddressDto) o;

    return new EqualsBuilder().append(id, address1.id).append(country, address1.country)
        .append(city, address1.city).append(streetName, address1.streetName)
        .append(houseNumber, address1.houseNumber)
        .append(apartNumber, address1.apartNumber).append(zipCode, address1.zipCode)
        .append(creationTime, address1.creationTime).append(lastChangeTime, address1.lastChangeTime).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(country).append(city).append(streetName)
        .append(houseNumber).append(apartNumber).append(zipCode).append(creationTime)
        .append(lastChangeTime).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("country", country).append("city",
        city)
        .append("streetName", streetName).append("houseNumber", houseNumber)
        .append("apartNumber", apartNumber).append("zipCode", zipCode)
        .append("creationTime", creationTime).append("lastChangeTime", lastChangeTime).toString();
  }
}
