package com.itreflection.invoicer.repository.api.dto;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.api.entities.Address;
import com.itreflection.invoicer.repository.api.entities.BankAccount;

/**
 * Created by sscode on 2017-01-28.
 */

public class CompanyDto {

  private long id;
  private Account account;
  private String name;
  private String taxId;
  private String ueTaxId;
  private Address address;
  private String phoneNumber1;
  private String phoneNumber2;
  private String fax;
  private String email;
  private String websiteUrl;
  private BankAccount backAccount;
  private DateTime created;
  private DateTime updated;

  public CompanyDto() {
  }

  public CompanyDto(Account account, String name, String taxId, String ueTaxId, Address address, String phoneNumber1,
      String phoneNumber2, String fax, String email, String websiteUrl, BankAccount backAccount, DateTime created,
      DateTime updated) {
    this.account = account;
    this.name = name;
    this.taxId = taxId;
    this.ueTaxId = ueTaxId;
    this.address = address;
    this.phoneNumber1 = phoneNumber1;
    this.phoneNumber2 = phoneNumber2;
    this.fax = fax;
    this.email = email;
    this.websiteUrl = websiteUrl;
    this.backAccount = backAccount;
    this.created = created;
    this.updated = updated;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Account getAccountId() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTaxId() {
    return taxId;
  }

  public void setTaxId(String taxId) {
    this.taxId = taxId;
  }

  public String getUeTaxId() {
    return ueTaxId;
  }

  public void setUeTaxId(String ueTaxId) {
    this.ueTaxId = ueTaxId;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getPhoneNumber1() {
    return phoneNumber1;
  }

  public void setPhoneNumber1(String phoneNumber1) {
    this.phoneNumber1 = phoneNumber1;
  }

  public String getPhoneNumber2() {
    return phoneNumber2;
  }

  public void setPhoneNumber2(String phoneNumber2) {
    this.phoneNumber2 = phoneNumber2;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getWebsiteUrl() {
    return websiteUrl;
  }

  public void setWebsiteUrl(String websiteUrl) {
    this.websiteUrl = websiteUrl;
  }

  public BankAccount getBackAccount() {
    return backAccount;
  }

  public void setBackAccount(BankAccount backAccount) {
    this.backAccount = backAccount;
  }

  public DateTime getCreated() {
    return created;
  }

  public void setCreated(DateTime created) {
    this.created = created;
  }

  public DateTime getUpdated() {
    return updated;
  }

  public void setUpdated(DateTime updated) {
    this.updated = updated;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;

    if (o == null)
      return false;

    if (o.getClass() != getClass())
      return false;

    CompanyDto company = (CompanyDto) o;

    return new EqualsBuilder().append(id, company.id).append(account, company.account).append(name, company.name)
        .append(taxId, company.taxId).append(ueTaxId, company.ueTaxId).append(address, company.address)
        .append(phoneNumber1, company.phoneNumber1).append(phoneNumber2, company.phoneNumber2).append(fax, company.fax)
        .append(email, company.email).append(websiteUrl, company.websiteUrl).append(backAccount, company.backAccount)
        .append(created, company.created).append(updated, company.updated).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).append(account).append(name).append(taxId).append(ueTaxId).append(address)
        .append(phoneNumber1).append(phoneNumber2).append(fax).append(email).append(websiteUrl).append(backAccount)
        .append(created).append(updated).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("account", account)
        .append("name", name).append("taxId", taxId).append("ueTaxId", ueTaxId).append("address", address)
        .append("phoneNumber1", phoneNumber1).append("phoneNumber2", phoneNumber2).append("fax", fax)
        .append("email", email).append("websiteUrl", websiteUrl).append("backAccount", backAccount)
        .append("created", created).append("updated", updated).toString();
  }

  @PrePersist
  protected void onCreate() {
    created = DateTime.now(DateTimeZone.UTC);
  }

  @PreUpdate
  protected void onUpdate() {
    updated = DateTime.now(DateTimeZone.UTC);
  }

}
