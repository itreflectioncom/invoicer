package com.itreflection.invoicer.repository.impl.service.account;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.itreflection.invoicer.repository.api.converter.Converter;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.impl.repository.AccountRepository;

@RunWith(MockitoJUnitRunner.class)
public class AccountRepositoryServiceImplTest {

   public static final String USERNAME = "name";
   public static final String EMAIL = "test@email.com";
   public static final String PASSWORD = "password";
   @InjectMocks
   private AccountRepositoryServiceImpl accountRepositoryService;

   @Mock
   private Converter<Account, AccountDto> accountConverter;
   @Mock
   private AccountRepository accountRepository;

   @Test
   public void shouldSaveAccount() throws Exception {
      //given
      AccountDto dto = new AccountDto();
      dto.setId(1L);
      dto.setUsername(USERNAME);
      dto.setEmail(EMAIL);
      dto.setPassword(PASSWORD);
      Account entity = mock(Account.class);

      when(accountConverter.convertFrom(dto)).thenReturn(entity);
      when(accountRepository.save(entity)).thenReturn(entity);
      when(accountConverter.convertTo(entity)).thenReturn(dto);

      //when
      AccountDto saved = accountRepositoryService.save(dto);

      //then
      verify(accountConverter).convertFrom(dto);
      verify(accountConverter).convertTo(entity);
      assertThat(saved).isEqualTo(dto);

   }

   @Test
   public void shouldFindByName() throws Exception {
      //given
      AccountDto dto = mock(AccountDto.class);
      Account entity = mock(Account.class);

      when(accountRepository.findByUsername(USERNAME)).thenReturn(entity);
      when(accountConverter.convertTo(entity)).thenReturn(dto);

      //when
      AccountDto found = accountRepositoryService.findByName(USERNAME);

      //then
      verify(accountConverter).convertTo(entity);
      assertThat(found).isEqualTo(dto);

   }

}