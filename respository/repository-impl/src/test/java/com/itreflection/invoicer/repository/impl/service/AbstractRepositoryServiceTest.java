package com.itreflection.invoicer.repository.impl.service;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.impl.repository.AccountRepository;

@RunWith(MockitoJUnitRunner.class)
public class AbstractRepositoryServiceTest {

   @InjectMocks
   private TestRepositoryService testRepositoryService;

   @Mock
   private AccountRepository accountRepository;

   @Test
   public void shouldSave() throws Exception {
      //given
      long id = 1L;
      AccountDto dto = new AccountDto();
      dto.setId(id);
      Account entity = new Account();
      entity.setId(id);

      when(accountRepository.save(any(Account.class))).thenReturn(entity);

      //when
      AccountDto saved = testRepositoryService.save(dto);

      //then
      verify(accountRepository).save(any(Account.class));
      assertThat(saved).isEqualTo(dto);
   }

   @Test
   public void shouldGetById() throws Exception {
      //given
      long id = 1L;
      Account entity = new Account();
      entity.setId(id);

      when(accountRepository.findOne(id)).thenReturn(entity);

      //when
      AccountDto dto = testRepositoryService.getById(id);

      //then
      verify(accountRepository).findOne(id);
      assertThat(dto).isNotNull();
      assertThat(dto.getId()).isEqualTo(id);
   }

   @Test
   public void shouldFindAll() throws Exception {
      //given
      long id = 1L;
      long id2 = 2L;
      Account account = new Account();
      account.setId(id);
      Account account2 = new Account();
      account2.setId(id2);

      when(accountRepository.findAll()).thenReturn(Lists.newArrayList(account, account2));

      //when
      List<AccountDto> result = testRepositoryService.findAll();

      //then
      verify(accountRepository).findAll();
      assertThat(result).hasSize(2);
      assertThat(result.get(0).getId()).isEqualTo(account.getId());
      assertThat(result.get(1).getId()).isEqualTo(account2.getId());

   }

}
