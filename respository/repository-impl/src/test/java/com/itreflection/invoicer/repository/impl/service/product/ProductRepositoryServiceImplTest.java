package com.itreflection.invoicer.repository.impl.service.product;

import com.itreflection.invoicer.repository.api.converter.Converter;
import com.itreflection.invoicer.repository.api.dto.ProductDto;
import com.itreflection.invoicer.repository.api.entities.Product;
import com.itreflection.invoicer.repository.api.entities.Status;
import com.itreflection.invoicer.repository.impl.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductRepositoryServiceImplTest {

   public static final String NAME = "name";
   public static final String CODE = "Test code";
   public static final String DESCRIPTION = "Test description";
   public static final Double PRICE = 1.00;
   public static final Integer TAX = 1;
   public static final Integer DISCOUNT = 1;
   public static final Double QUANTITY = 1.00;
   public static final String QUANTITY_UNIT = "KG";
   @InjectMocks
   private ProductRepositoryServiceImpl productRepositoryService;

   @Mock
   private Converter<Product, ProductDto> productConverter;
   @Mock
   private ProductRepository productRepository;

   @Test
   public void shouldSaveProduct() throws Exception {
      //given
      ProductDto dto = new ProductDto();
      dto.setId(1L);
      dto.setName(NAME);
      dto.setCode(CODE);
      dto.setDescription(DESCRIPTION);
      dto.setStatus(Status.PREDEFINED);
      dto.setDefaultPrice(PRICE);
      dto.setDefaultTaxes(TAX);
      dto.setDefaultDiscount(DISCOUNT);
      dto.setQuantity(QUANTITY);
      dto.setQuantityUnit(QUANTITY_UNIT);
      Product entity = mock(Product.class);

      when(productConverter.convertFrom(dto)).thenReturn(entity);
      when(productRepository.save(entity)).thenReturn(entity);
      when(productConverter.convertTo(entity)).thenReturn(dto);

      //when
      ProductDto saved = productRepositoryService.save(dto);

      //then
      verify(productConverter).convertFrom(dto);
      verify(productConverter).convertTo(entity);
      assertThat(saved).isEqualTo(dto);

   }

   @Test
   public void shouldFindByName() throws Exception {
      //given
      ProductDto dto = mock(ProductDto.class);
      Product entity = mock(Product.class);

      when(productRepository.findByName(NAME)).thenReturn(entity);
      when(productConverter.convertTo(entity)).thenReturn(dto);

      //when
      ProductDto found = productRepositoryService.findByName(NAME);

      //then
      verify(productConverter).convertTo(entity);
      assertThat(found).isEqualTo(dto);

   }

}