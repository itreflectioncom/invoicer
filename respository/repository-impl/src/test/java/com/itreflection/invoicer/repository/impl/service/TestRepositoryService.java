package com.itreflection.invoicer.repository.impl.service;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.impl.repository.AccountRepository;

public class TestRepositoryService extends AbstractRepositoryService<AccountDto, Account, Long, AccountRepository> {

   public TestRepositoryService(AccountRepository repository) {
      super(repository);
   }

   @Override
   protected Account convertToEntity(AccountDto dto) {
      return new Account();
   }

   @Override
   protected AccountDto convertToDto(Account entity) {
      AccountDto accountDto = new AccountDto();
      accountDto.setId(entity.getId());
      return accountDto;
   }

   @Override
   public void delete(Long id) {

   }
}
