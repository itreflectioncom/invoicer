package com.itreflection.invoicer.repository.impl.converter.account;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.api.entities.Authority;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;

@RunWith(MockitoJUnitRunner.class)
public class AccountConverterImplTest {

  @InjectMocks
  private AccountConverterImpl accountConverter;

  @Test
  public void shouldConvertTo() {
    Account account = new Account();
    AccountDto accountDto;

    setupAccount(account);

    accountDto = accountConverter.convertTo(account);

    assertThat(account.getId()).isEqualTo(accountDto.getId());
    assertThat(account.getUsername()).isEqualTo(accountDto.getUsername());
    assertThat(account.getEmail()).isEqualTo(accountDto.getEmail());
    assertThat(account.getPassword()).isEqualTo(accountDto.getPassword());
    assertThat(account.getCreated()).isEqualTo(accountDto.getCreated());
    assertThat(account.getUpdated()).isEqualTo(accountDto.getUpdated());
    assertThat(account.getLastLoginTime()).isEqualTo(accountDto.getLastLoginTime());

    List<AuthorityType> authorityTypes = new ArrayList<>();

    account.getAuthorities().forEach(authority -> authorityTypes.add(authority.getAuthority()));

    assertThat(authorityTypes).isEqualTo(accountDto.getAuthorities());

  }

  @Test
  public void shouldReturnNullConvertTo() {
    Account account = null;

    assertThat(accountConverter.convertTo(account)).isNull();
  }

  @Test
  public void shouldReturnNullConvertFrom() {
    AccountDto accountDto = null;

    assertThat(accountConverter.convertFrom(accountDto)).isNull();
  }

  @Test
  public void shouldConvertFrom() {
    Account account;
    AccountDto accountDto = new AccountDto();

    setupAccountDto(accountDto);

    account = accountConverter.convertFrom(accountDto);

    assertThat(accountDto.getId()).isEqualTo(account.getId());
    assertThat(accountDto.getUsername()).isEqualTo(account.getUsername());
    assertThat(accountDto.getEmail()).isEqualTo(account.getEmail());
    assertThat(accountDto.getPassword()).isEqualTo(account.getPassword());
    assertThat(accountDto.getCreated()).isEqualTo(account.getCreated());
    assertThat(accountDto.getUpdated()).isEqualTo(account.getUpdated());
    assertThat(accountDto.getLastLoginTime()).isEqualTo(account.getLastLoginTime());
  }

  private void setupAccount(Account account) {
    account.setId(1L);
    account.setUsername("test");
    account.setEmail("test@test.com");
    account.setPassword("testest");
    account.setCreated(new DateTime());
    account.setUpdated(new DateTime());
    account.setLastLoginTime(new DateTime());

    List<Authority> authorities = new ArrayList<>();
    Authority authority1 = new Authority(account.getId(), AuthorityType.VIEW_ACCOUNT);
    Authority authority2 = new Authority(account.getId(), AuthorityType.MANAGE_ACCOUNT);
    authorities.add(authority1);
    authorities.add(authority2);

    account.setAuthorities(authorities);
  }

  private void setupAccountDto(AccountDto accountDto) {
    accountDto.setId(1L);
    accountDto.setUsername("test");
    accountDto.setEmail("test@test.com");
    accountDto.setPassword("testest");
    accountDto.setCreated(new DateTime());
    accountDto.setUpdated(new DateTime());
    accountDto.setLastLoginTime(new DateTime());

    List<AuthorityType> authorityTypes = new ArrayList<>();
    authorityTypes.add(AuthorityType.VIEW_ACCOUNT);
    authorityTypes.add(AuthorityType.MANAGE_ACCOUNT);

    accountDto.setAuthorities(authorityTypes);
  }

}
