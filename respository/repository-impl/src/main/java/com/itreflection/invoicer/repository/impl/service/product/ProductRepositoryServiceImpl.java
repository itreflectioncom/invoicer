package com.itreflection.invoicer.repository.impl.service.product;

import com.itreflection.invoicer.repository.api.converter.Converter;
import com.itreflection.invoicer.repository.api.dto.ProductDto;
import com.itreflection.invoicer.repository.api.entities.Product;
import com.itreflection.invoicer.repository.api.service.product.ProductRepositoryService;
import com.itreflection.invoicer.repository.impl.repository.ProductRepository;
import com.itreflection.invoicer.repository.impl.service.AbstractRepositoryService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class ProductRepositoryServiceImpl
    extends AbstractRepositoryService<ProductDto, Product, Long, ProductRepository>
    implements ProductRepositoryService {

   private final Logger logger = getLogger(ProductRepositoryServiceImpl.class);

   private final Converter<Product, ProductDto> productConverter;

   @Autowired
   public ProductRepositoryServiceImpl(ProductRepository repository, Converter<Product, ProductDto> productConverter) {
      super(repository);
      this.productConverter = productConverter;
   }

   @Override
   public ProductDto findByName(String name) {
      Product entity = repository.findByName(name);
      logger.info("Found product for name: {} -> {}", name, entity);
      return convertToDto(entity);
   }

   @Override
   public void delete(Long id) {
      repository.delete(id);
      logger.info("Product with id: {} is deleted.", id);
   }

   @Override
   protected Product convertToEntity(ProductDto dto) {
      return productConverter.convertFrom(dto);
   }

   @Override
   protected ProductDto convertToDto(Product entity) {
      return productConverter.convertTo(entity);
   }
}
