package com.itreflection.invoicer.repository.impl.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itreflection.invoicer.repository.api.entities.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

  Account findByUsername(String name);
}
