package com.itreflection.invoicer.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.support.ClasspathScanningPersistenceUnitPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan
@EnableTransactionManagement
public class JPAConfiguration {

  @Autowired
  private DataSource dataSource;

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setDataSource(dataSource);
    factory.setPackagesToScan("com.itreflection.repository.api.domain");

     HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
     vendorAdapter.setShowSql(false);
     vendorAdapter.setGenerateDdl(true);
     vendorAdapter.setDatabase(Database.H2);
     vendorAdapter.setDatabasePlatform(org.hibernate.dialect.H2Dialect.class.getCanonicalName());

    factory.setJpaVendorAdapter(vendorAdapter);
    factory.setJpaProperties(configureJPA());
    factory.setPersistenceUnitName("invoicer");
    factory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
    ClasspathScanningPersistenceUnitPostProcessor persistencePostProcessor = new ClasspathScanningPersistenceUnitPostProcessor(
        "com.itreflection.invoicer.repository.api.entities");
    factory.setPersistenceUnitPostProcessors(persistencePostProcessor);

    return factory;
  }

  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  @Bean
  public PlatformTransactionManager transactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    EntityManagerFactory entityManagerFactoryBean = entityManagerFactory().getObject();
    transactionManager.setEntityManagerFactory(entityManagerFactoryBean);
    transactionManager.setJpaDialect(new HibernateJpaDialect());

    return transactionManager;
  }

  private Properties configureJPA() {
    Properties properties = new Properties();
    properties.put(Environment.HBM2DDL_AUTO, "create");
    properties.put("hibernate.archive.autodetection", "class,hbm");
    properties.put("hibernate.show_sql", "false");
    properties.put(Environment.DIALECT, org.hibernate.dialect.H2Dialect.class.getCanonicalName());
    return properties;
  }
}
