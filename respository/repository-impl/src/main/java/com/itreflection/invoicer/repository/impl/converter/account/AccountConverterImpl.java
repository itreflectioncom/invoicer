package com.itreflection.invoicer.repository.impl.converter.account;

import java.util.List;

import org.springframework.stereotype.Service;

import com.itreflection.invoicer.repository.api.converter.Converter;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.api.entities.Authority;

@Service
public class AccountConverterImpl implements Converter<Account, AccountDto> {

  @Override
  public AccountDto convertTo(Account src) {
    if (src == null) {
      return null;
    }

    AccountDto target = new AccountDto();
    target.setId(src.getId());
    target.setUsername(src.getUsername());
    target.setEmail(src.getEmail());
    target.setPassword(src.getPassword());

    List<Authority> authorities = src.getAuthorities();
    if (authorities != null) {
      authorities.forEach(item -> target.getAuthorities().add(item.getAuthority()));
    }

    target.setCreated(src.getCreated());
    target.setUpdated(src.getUpdated());
    target.setLastLoginTime(src.getLastLoginTime());

    return target;
  }

  @Override
  public Account convertFrom(AccountDto src) {
    if (src == null) {
      return null;
    }

    Account target = new Account();
    target.setId(src.getId());
    target.setUsername(src.getUsername());
    target.setEmail(src.getEmail());
    target.setPassword(src.getPassword());
    target.setCreated(src.getCreated());
    target.setUpdated(src.getUpdated());
    target.setLastLoginTime(src.getLastLoginTime());

    return target;
  }

}
