package com.itreflection.invoicer.repository.impl.service.account;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itreflection.invoicer.repository.api.converter.Converter;
import com.itreflection.invoicer.repository.api.dto.AccountDto;
import com.itreflection.invoicer.repository.api.entities.Account;
import com.itreflection.invoicer.repository.api.entities.Authority;
import com.itreflection.invoicer.repository.api.entities.AuthorityType;
import com.itreflection.invoicer.repository.api.service.account.AccountRepositoryService;
import com.itreflection.invoicer.repository.impl.repository.AccountRepository;
import com.itreflection.invoicer.repository.impl.service.AbstractRepositoryService;

@Transactional
@Service
public class AccountRepositoryServiceImpl
    extends AbstractRepositoryService<AccountDto, Account, Long, AccountRepository>
    implements AccountRepositoryService {

  private final Logger logger = getLogger(AccountRepositoryServiceImpl.class);

  private final Converter<Account, AccountDto> accountConverter;

  @Autowired
  public AccountRepositoryServiceImpl(AccountRepository repository, Converter<Account, AccountDto> accountConverter) {
    super(repository);
    this.accountConverter = accountConverter;
  }

  @Override
  public AccountDto findByName(String name) {
    Account entity = repository.findByUsername(name);
    logger.info("Found account for name: {} -> {}", name, entity);
    return convertToDto(entity);
  }

  @Override
  public void delete(Long id) {
    repository.delete(id);
    logger.info("Account with id: {} is deleted.", id);
  }

  @Override
  public AccountDto save(AccountDto dto) {
    logger.info("Saving object: {}", dto);
    Account entity = convertToEntity(dto);
    Account savedWithoutAuthorities = repository.save(entity);

    Account saved = saveAuthorities(dto, savedWithoutAuthorities);
    return convertToDto(saved);
  }

  private Account saveAuthorities(AccountDto dto, Account savedWithoutAuthorities) {
    List<AuthorityType> authorities = dto.getAuthorities();
    if (authorities != null) {
      authorities.forEach(item -> savedWithoutAuthorities.getAuthorities().add(new Authority(savedWithoutAuthorities.getId(), item)));
    }
    return repository.save(savedWithoutAuthorities);
  }

  @Override
  protected Account convertToEntity(AccountDto dto) {
    return accountConverter.convertFrom(dto);
  }

  @Override
  protected AccountDto convertToDto(Account entity) {
    return accountConverter.convertTo(entity);
  }
}
