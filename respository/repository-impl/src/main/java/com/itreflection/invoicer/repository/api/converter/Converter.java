package com.itreflection.invoicer.repository.api.converter;

public interface Converter<K, V> {

  V convertTo(K src);

  K convertFrom(V src);
}
