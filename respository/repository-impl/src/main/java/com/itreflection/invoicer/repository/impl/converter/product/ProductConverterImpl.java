package com.itreflection.invoicer.repository.impl.converter.product;

import com.itreflection.invoicer.repository.api.converter.Converter;
import com.itreflection.invoicer.repository.api.dto.ProductDto;
import com.itreflection.invoicer.repository.api.entities.Product;
import org.springframework.stereotype.Service;

@Service
public class ProductConverterImpl implements Converter<Product, ProductDto> {

  @Override
  public ProductDto convertTo(Product src) {
    if (src == null) {
      return null;
    }

    ProductDto target = new ProductDto();
    target.setId(src.getId());
    target.setType(src.getType());
    target.setStatus(src.getStatus());
    target.setName(src.getName());
    target.setCode(src.getCode());
    target.setDescription(src.getDescription());
    target.setDefaultPrice(src.getDefaultPrice());
    target.setDefaultTaxes(src.getDefaultTaxes());
    target.setDefaultDiscount(src.getDefaultDiscount());
    target.setQuantity(src.getQuantity());
    target.setQuantityUnit(src.getQuantityUnit());

    return target;
  }

  @Override
  public Product convertFrom(ProductDto src) {
    if (src == null) {
      return null;
    }

    Product target = new Product();
    target.setId(src.getId());
    target.setType(src.getType());
    target.setStatus(src.getStatus());
    target.setName(src.getName());
    target.setCode(src.getCode());
    target.setDescription(src.getDescription());
    target.setDefaultPrice(src.getDefaultPrice());
    target.setDefaultTaxes(src.getDefaultTaxes());
    target.setDefaultDiscount(src.getDefaultDiscount());
    target.setQuantity(src.getQuantity());
    target.setQuantityUnit(src.getQuantityUnit());

    return target;
  }

}
