package com.itreflection.invoicer.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@EnableJpaRepositories(basePackages = { "com.itreflection.invoicer.repository.impl.repository" })
@ComponentScan(basePackages = { "com.itreflection.invoicer.repository.impl" })
@Import(JPAConfiguration.class)
public class RepositoryConfiguration {

  @Bean
  public DataSource dataSource() {
    return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).setName("integration-test")
        //        .addScript("db/ddl.sql").addScript("db/dml.sql")
        .build();
  }

  //  @Bean
  //  public DataSource dataSource() {
  //    final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
  //    dsLookup.setResourceRef(true);
  //    return dsLookup.getDataSource("java:/datasources/invoicer");
  //  }

}
