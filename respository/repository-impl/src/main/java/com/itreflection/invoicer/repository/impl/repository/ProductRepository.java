package com.itreflection.invoicer.repository.impl.repository;


import com.itreflection.invoicer.repository.api.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

  Product findByName(String name);
}
